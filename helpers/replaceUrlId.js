const replaceUrlId = (endpoint = '', object = {}) => {
  let newUrl = endpoint;
  for (var key of Object.keys(object)) {
    newUrl = newUrl.replace(`{${key}}`, object[key]);
  }
  return newUrl;
};
export default replaceUrlId;
