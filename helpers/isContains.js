const isContains = (path = '', target = '') => {
  /**
   *
   * alternative of String includes() for internet-explorer compatibility
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes#browser_compatibility
   *
   */

  return path.indexOf(target) !== -1;
};

export default isContains;
