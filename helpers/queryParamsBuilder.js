export const queryParamsBuilder = params => {
  let queryParams = '';
  for (let key in params) {
    // currently BE don't treat filter and keyword empty string as optional which strangely lead to cors error or internal 500 error.
    // if (!String(params[key])) continue;
    queryParams = queryParams.concat(`${queryParams ? '&' : '?'}${key}=${String(params[key])}`);
  }
  return queryParams;
};

export const generateSearchUrl = (endpoint = '', params) => {
  const queryParams = queryParamsBuilder(params);
  return `${endpoint}${queryParams}`;
};
