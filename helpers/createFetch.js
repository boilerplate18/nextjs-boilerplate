/* eslint-disable */
const KEY_TOKEN = 'XxfOtMSMhjoMQEPUHju4u7jkCEpZ09';
const MUTATION_REQUESTS = ['POST', 'PUT'];

const createFetch = (urlScheme, options = {}) => {
  const { authToken, method, contentType, endpoint, ssr } = options;
  return (key = '', variables = {}) => {
    const usedVariables = MUTATION_REQUESTS.includes(method) ? key : variables;

    let finalUrl = '';

    if (usedVariables && typeof urlScheme === 'function') {
      finalUrl = urlScheme(endpoint, usedVariables);
    } else {
      finalUrl = String(urlScheme);
    }

    let signal;
    let controller;
    let formData;
    if (!ssr) {
      controller = new AbortController();
      signal = controller.signal;

      // create form-data
      formData = new FormData();
      if (usedVariables && contentType === 'form-data') {
        const keys = Object.keys(usedVariables);
        for (const key of keys) {
          formData.append(key, usedVariables[key]);
        }
      }
    }

    const cType = contentType === 'form-data' ? {} : { 'Content-Type': 'application/json' };
    const mthd = { method: method };

    const promise = fetch(finalUrl, {
      ...(!ssr && { signal: signal }),
      ...mthd,
      ...(MUTATION_REQUESTS.includes(method) && {
        body: contentType === 'form-data' ? formData : JSON.stringify(usedVariables),
      }),
      headers: {
        Accept: 'application/json, text/plain, */*',
        Key: KEY_TOKEN,
        // ...(cookie?.token && { Authorization: `Bearer ${cookie.token}` }),
        ...cType,
        ...(authToken && { Authorization: authToken }),
      },
    })
      .then(res => {
        if (!ssr) {
          switch (res?.status) {
            case 401:
              if (!!authToken) window.location.assign('/logout');
              break;
            case 403:
              // window.location.assign('/unauthorized');
              break;
          }
        }

        return res.json();
      })
      .catch(e => {
        console.error(e);
      });

    if (!ssr) promise.cancel = controller.abort;

    return promise;
  };
};

export default createFetch;
