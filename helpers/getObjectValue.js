export function getObjectValue(object, path = '') {
  const pathArr = path.split('.');
  const obj = pathArr.reduce((o, x, i) => {
    if (o[x]) return o[x];
    else if (!o[x] && i === pathArr.length - 1) {
      return {};
    } else return o;
  }, object);
  if (JSON.stringify(obj) === '{}' || object === obj) return '';
  return obj;
}
