import LoginForm from '@components/login/LoginForm';
import Head from 'next/head';

const bg = '/assets/bg/maps-bg-image.svg';

export default function Login() {
  return (
    <>
      <Head>
        <title>Login Page</title>
      </Head>

      <div className="flex flex-row h-screen">
        <div className="hidden md:flex flex-col w-7/12 bg-login-page ">
          <div className="flex mx-auto justify-center flex-col h-2/6 w-full">
            <h2 className="text-center mx-auto text-white mt-14 text-md">Welcome Back!</h2>
            <p className="w-80 text-center mx-auto mt-2 text-white text-sm">
              Please insert your Username and Password to log in Datatec System
            </p>
          </div>
          <div className="flex h-4/6 w-full mx-auto">
            <div className="flex h-full w-full bg-icon "></div>
          </div>
        </div>

        <div className="flex w-full justify-center lg:w-5/12 items-center bg-white">
          <div className="flex flex-col  mx-auto h-auto w-9/12">
            <h1 className="mx-auto text-center lg:text-2xl text-primary-lightblue">Login</h1>
            <LoginForm />
          </div>
        </div>
        <style jsx>{`
          .bg-login-page {
            background: linear-gradient(-135deg, rgb(104, 186, 253) 0%, rgb(55, 132, 251) 100%);
          }

          .bg-icon {
            background-image: url(${bg});
            background-repeat: none;
            background-size: cover;
            margin: 0 auto;
          }
        `}</style>
      </div>
    </>
  );
}
