import { Button } from '@mui/material';
import DriveEtaIcon from '@mui/icons-material/DriveEta';

import { AlertProvider } from '@context/alert';
import List from '@components/user-admin/List';
import UserForm from '@components/user-admin/Form';
import { useModal } from '@context/modal';

const UserList = () => {
  const { openModal } = useModal();

  const handleOpen = () => {
    openModal({
      title: 'Add Admin',
      icon: <DriveEtaIcon className="text-white" />,
      content: <UserForm />,
    });
  };
  return (
    <>
      <div className="flex flex-row justify-between mb-8">
        <h1 className="text-primary-darkblue">Admin</h1>
        <Button type="button" variant="contained" color="primary" onClick={handleOpen}>
          <DriveEtaIcon className="mr-2" /> Add Admin
        </Button>
      </div>
      <AlertProvider>
        <List />
      </AlertProvider>
    </>
  );
};
export default UserList;
