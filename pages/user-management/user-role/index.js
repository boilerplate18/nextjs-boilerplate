import { Button } from '@mui/material';
import DriveEtaIcon from '@mui/icons-material/DriveEta';

import { useModal } from '@context/modal';
import List from '@components/user-role/List';
import UserForm from '@components/user-role/Form';
import { AlertProvider } from '@context/alert';

const UserList = () => {
  const { openModal } = useModal();

  const handleOpen = () => {
    openModal({
      title: 'Add User Role',
      icon: <DriveEtaIcon className="text-white" />,
      content: <UserForm />,
    });
  };
  return (
    <>
      <div className="flex flex-row justify-between mb-8">
        <h1 className="text-primary-darkblue">User Role</h1>
        <Button type="button" variant="contained" color="primary" onClick={handleOpen}>
          <DriveEtaIcon className="mr-2" /> Add User Role
        </Button>
      </div>
      <AlertProvider>
        <List />
      </AlertProvider>
    </>
  );
};
export default UserList;
