import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

const login = async data => {
  const response = await fetch(`${process.env.RESTAPI_PROXIES_HOSTNAME}/api/login`, {
    method: 'POST',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json',
      Key: 'XxfOtMSMhjoMQEPUHju4u7jkCEpZ09',
    },
    body: JSON.stringify(data),
  });
  return response;
};
export default NextAuth({
  providers: [
    CredentialsProvider({
      id: 'username-login',
      name: 'username-login',
      credentials: {
        username: { label: 'Username', type: 'text', placeholder: 'jsmith' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        try {
          // Add logic here to look up the user from the credentials supplied
          const data = {
            email: credentials.username,
            password: credentials.password,
          };
          const res = await login(data);

          const user = await res.json();
          if (res.ok && user) {
            // Any object returned will be saved in `user` property of the JWT
            return user?.data;
          } else {
            // If you return null or false then the credentials will be rejected
            throw new Error(user?.message || 'Whoops ! Something went wrong !');
            // You can also Reject this callback with an Error or with a URL:
            // throw new Error('error message') // Redirect to error page
            // throw '/path/to/redirect'        // Redirect to a URL
          }
        } catch (error) {
          throw new Error(error);
        }
      },
    }),
  ],
  site: process.env.NEXTAUTH_URL || 'http://localhost:3000',
  session: {
    jwt: true,
  },
  callbacks: {
    async signIn({ user }) {
      if (user) {
        return true;
      } else {
        return false;
      }
    },
    async redirect({ baseUrl }) {
      return baseUrl;
    },
    async jwt({ token, user }) {
      //  "token" is being send below to "session" callback, so we set "user" param of "token" to object from "authorize", and return it
      if (user) return { ...token, ...user };
      else return token;
    },
    async session({ session, token }) {
      session.token = token;
      console.log('check session', session, token);
      return session;
    },
  },
  pages: {
    signIn: '/auth/login',
    signOut: '/auth/logout',
    error: '/auth/error',
  },

  debug: process.env.NODE_ENV === 'development',
  secret: process.env.NEXT_PUBLIC_AUTH_SECRET,
  jwt: {
    // secret: process.env.NEXT_PUBLIC_JWT_SECRET,
    signingKey: process.env.JWT_SIGNING_PRIVATE_KEY,
  },
});
