import { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { SessionProvider } from 'next-auth/react';
// import { CacheProvider } from '@emotion/react';
import CssBaseline from '@mui/material/CssBaseline';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';
// import * as gtag from '@helpers/gtag';

import { ReactQueryDevtools } from 'react-query/devtools';

import AppProvider from '@context/index';
import { QueryClientProvider, QueryClient } from 'react-query';

import SiteLayout from '@components/common/Layout';
import { MaterialThemeProvider } from '@components/common/Layout/core/MaterialThemeProvider';
import ErrorBoundary from '@components/common/ErrorBoundary';
import ErrorView from '@components/common/ErrorView';
import ModalForm from '@components/common/ModalForm';

// import createEmotionCache from '@helpers/createEmotionCache';
import 'react-toastify/dist/ReactToastify.min.css';
import 'tailwindcss/tailwind.css';
import '@styles/styles.css';

// const clientSideEmotionCache = createEmotionCache();
function MyApp({ Component, pageProps }) {
  const [isOnline, setIsOnline] = useState(true);
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        throwOnError: true,
        refetchAllOnWindowFocus: false,
        refetchOnMount: true,
        retry: 3,
      },
      mutations: {
        // mutation options
      },
    },
  });

  useEffect(() => {
    if (typeof window !== 'undefined' && 'ononline' in window && 'onoffline' in window) {
      setIsOnline(window.navigator.onLine);
      if (!window.ononline) {
        window.addEventListener('online', () => {
          setIsOnline(true);
        });
      }
      if (!window.onoffline) {
        window.addEventListener('offline', () => {
          setIsOnline(false);
        });
      }
    }
  }, []);

  const router = useRouter();
  useEffect(() => {
    if (typeof window !== 'undefined' && 'serviceWorker' in navigator && window.workbox !== undefined && isOnline) {
      // skip index route, because it's already cached under `start-url` caching object
      // skip package route, because it makes script execution time longer
      if (router.route !== '/' && router.route !== '/package') {
        const wb = window.workbox;
        wb.active.then(() => {
          wb.messageSW({ action: 'CACHE_NEW_ROUTE' });
        });
      }
    }
  }, [isOnline, router.route]);
  // useEffect(() => {
  //   const handleRouteChange = url => {
  //     gtag.pageview(url);
  //   };
  //   router.events.on('routeChangeComplete', handleRouteChange);
  //   return () => {
  //     router.events.off('routeChangeComplete', handleRouteChange);
  //   };
  // }, [router.events]);

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover"
        />
      </Head>

      <MaterialThemeProvider>
        <CssBaseline />
        <ErrorBoundary render={() => <ErrorView />}>
          <SessionProvider
            options={{
              staleTime: 0,
              refetchInterval: 0,
            }}
            session={pageProps.session}
          >
            <QueryClientProvider client={queryClient}>
              {Component.auth ? (
                <AppProvider>
                  <SiteLayout>
                    <Component {...pageProps} />
                  </SiteLayout>
                  <ModalForm />
                </AppProvider>
              ) : (
                <Component {...pageProps} />
              )}
              <ToastContainer closeButton={false} pauseOnHover={false} autoClose={3000} position="top-center" />
              <ReactQueryDevtools initialIsOpen={false} />
            </QueryClientProvider>
          </SessionProvider>
        </ErrorBoundary>
      </MaterialThemeProvider>
    </>
  );
}
MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object,
};

MyApp.defaultProps = {
  emotionCache: {},
  pageProps: {},
};
export default MyApp;
