import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { extractCritical } from '@emotion/server';

import { muiTheme } from '@components/common/Layout/core/MaterialThemeProvider';

const APP_NAME = 'Boilerplate';
const APP_DESCRIPTION = 'Desc';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="id">
        <Head>
          <link rel="preconnect" href={process.env.NEXTAUTH_URL} crossOrigin="anonymous" />
          <link rel="dns-prefetch" href={process.env.NEXTAUTH_URL} />

          <meta name="application-name" content={APP_NAME} />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="default" />
          <meta name="apple-mobile-web-app-title" content={APP_NAME} />
          <meta name="description" content={APP_DESCRIPTION} />
          <meta name="format-detection" content="telephone=no" />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="theme-color" content="#FFFFFF" />
          <link rel="apple-touch-icon" sizes="180x180" href="/icons/192x192.png" />
          <link rel="manifest" href="/manifest.json" />
          <link rel="shortcut icon" href="/favicon.ico" />

          {/* PWA primary color */}
          <meta name="theme-color" content={muiTheme.palette.primary.main} />

          {/* <link href="/fonts/Gotham-Book.woff2" as="font" type="font/woff2" crossOrigin="anonymous" />
          <link href="/fonts/Gotham-Bold.woff2" as="font" type="font/woff2" crossOrigin="anonymous" />
          <link href="/fonts/Gotham-Medium.woff2" as="font" type="font/woff2" crossOrigin="anonymous" />
          <link href="/fonts/gotham.css" as="style" />
          <link rel="stylesheet" href="/fonts/gotham.css" /> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

MyDocument.getInitialProps = async ctx => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  const initialProps = await Document.getInitialProps(ctx);
  const critical = extractCritical(initialProps.html);
  initialProps.html = critical.html;
  initialProps.styles = (
    <React.Fragment>
      {initialProps.styles}
      <style data-emotion-css={critical.ids.join(' ')} dangerouslySetInnerHTML={{ __html: critical.css }} />
    </React.Fragment>
  );

  return initialProps;
};
