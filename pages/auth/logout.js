import { useEffect } from 'react';
import { signOut } from 'next-auth/react';
import { LinearProgress } from '@mui/material';
import { withAuthRoute } from '@components/common/HOC';

import { useMutation } from 'react-query';
import createFetch from '@helpers/createFetch';
import { RESTAPI_HOST } from '@constants/index';
import { LOGOUT } from '@constants/endpoint/mutations';

const Logout = () => {
  const mutation = useMutation(createFetch(`${RESTAPI_HOST}${LOGOUT.path}`, { method: LOGOUT.method }));
  useEffect(() => {
    const logoutFunc = async () => {
      await mutation.mutateAsync({});
      if (typeof window !== 'undefined') localStorage.clear();
      signOut({ callbackUrl: `/auth/login` });
    };
    logoutFunc();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="flex w-full">
      <LinearProgress className="w-full" />
    </div>
  );
};
export default withAuthRoute(Logout);
