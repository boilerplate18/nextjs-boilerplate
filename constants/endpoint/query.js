/* Member Query */
export const USERS = {
  path: '/api/users',
  method: 'GET',
};
export const CREATE_USERS = {
  path: '/api/users/create',
  method: 'POST',
};
export const EDIT_USERS = {
  path: '/api/users/{userId}/edit',
  method: 'POST',
};
export const USER_ACCESS_LIST = {
  path: '/api/users/{userId}/access',
  method: 'GET',
};
export const DATA_MEMBER = {
  path: '/api/users/{userId}/access',
  method: 'GET',
};
