export const LOGIN = {
  path: '/api/login',
  method: 'POST',
};
export const LOGOUT = {
  path: '/api/logout',
  method: 'POST',
};
