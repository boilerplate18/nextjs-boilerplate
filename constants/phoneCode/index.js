import phoneCodeData from './phoneCode.json';

const phoneCodeKeys = Object.keys(phoneCodeData);

const mappedPhoneCodes = phoneCodeKeys.map(country => {
  return {
    ...phoneCodeData[country],
    country,
  };
});

export default mappedPhoneCodes;
