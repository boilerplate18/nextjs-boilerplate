import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

export const { RESTAPI_HOST } = publicRuntimeConfig;
