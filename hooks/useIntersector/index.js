import { useRef, useEffect } from 'react';

const useInterSector = (target, options = {}, callback = () => {}) => {
  const { once, dependency, ...opts } = options;
  const optsRef = useRef(opts);
  const intersectedRef = useRef(false);
  const shallowEqual = (object1, object2) => {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (let key of keys1) {
      if (object1[key] !== object2[key]) {
        return false;
      }
    }

    return true;
  };

  useEffect(() => {
    if (!shallowEqual(optsRef.current, opts)) {
      optsRef.current = opts;
    }
  });

  useEffect(() => {
    if (!target) {
      return;
    }

    const element = target instanceof Element ? target : target.current;
    if (!element) {
      return;
    }

    if (once && intersectedRef.current) {
      return;
    }

    const observer = new IntersectionObserver(
      entries => {
        const entry = entries[entries.length - 1];

        if (entry.isIntersecting) {
          intersectedRef.current = true;
          if (callback) callback();
        }

        if (once && entry.isIntersecting && element) {
          observer.unobserve(element);
        }
      },
      {
        ...optsRef.current,
        root: optsRef.current.root != null ? optsRef.current.root.current : null,
      },
    );
    observer.observe(element);
    return () => {
      if (once && intersectedRef.current) {
        return;
      }

      if (element != null) {
        observer.unobserve(element);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [optsRef.current, target, dependency]);
};

export default useInterSector;
