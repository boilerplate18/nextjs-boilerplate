const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  separator: ':',
  plugins: [require('tailwindcss'), require('precss'), require('autoprefixer'), require('@tailwindcss/aspect-ratio')],
  purge: {
    content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
    options: {
      safelist: ['lg:w-1/4', 'lg:w-1/3'],
    },
  },
  variants: {
    borderColor: ['hover', 'focus'],
  },
  important: true,
  theme: {
    screens: {
      sm: '576px',
      md: '768px',
      lg: '992px',
      xl: '1200px',
    },
    fontSize: {
      '2xs': '0.625rem', //10px
      xs: '0.75rem', //12px
      sm: '0.875rem', //14px
      '2sm': '0.9375rem', //15px
      base: '1rem', //16px
      lg: '1.125rem', //18px
      xl: '1.25rem', //20px
      '2xl': '1.375rem', //22px
      '3xl': '1.5rem', //24px
      // '3xl': '1.875rem',
      '4xl': '2rem', //32px
      '5xl': '3rem',
      '6xl': '4rem',
    },
    fontFamily: {
      sans: ['Tajawal', ...defaultTheme.fontFamily.sans],
    },
    maxWidth: {
      '1/4': '25%',
      '2/4': '50%',
      '3/4': '75%',
    },
    extend: {
      colors: {
        primary: {
          darkblue: '#022964',
          lightblue: '#3784fb',
          yellow: '#fbae04',
        },
        gray: {
          darkest: '#1b1b1b',
          dark: '#5e5e5e',
          DEFAULT: '#d9d7d7',
          light: '#e0e0e0',
          lightest: '#edecec',
        },
        secondary: {
          red: '#d21a1a',
          blue: '#406195',
        },
      },
    },
  },
};
