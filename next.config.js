const nextBuildId = require('next-build-id');
const withPlugins = require('next-compose-plugins');
const withSvgr = require('next-svgr');
const withImages = require('next-images');
const withPWA = require('next-pwa');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const { createSecureHeaders } = require('next-secure-headers');
const envConstants = require('./constants/getEnvConstants');

const config = {
  future: { webpack5: true },
  generateBuildId: () => nextBuildId({ dir: __dirname }),
  pwa: {
    dest: 'public',
    register: true,
    disable: process.env.NODE_ENV === 'development',
    // fallbacks: {
    //   document: '/fallback',
    //   image: '/assets/default-fallback-image.png',
    // },
    swSrc: 'service-worker.js',
  },
  webpackDevMiddleware: config => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };
    return config;
  },
  images: {
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
    domains: ['btpn-saml.logique.co.id', 'logique.co.id', 'btpn-customer.logique.co.id', 'btpn-api.logique.co.id'],
  },
  publicRuntimeConfig: {
    ...envConstants,
  },
  async headers() {
    return [
      {
        source: '/:all*(svg|jpg|png|webp)',
        locale: false,
        headers: [
          {
            key: 'Cache-Control',
            value: 'public, max-age=1, stale-while-revalidate=250',
            // value: 'public, max-age=9999999999, stale-while-revalidate=30',
          },
        ],
      },
      {
        // Apply these headers to all routes in your application.
        source: '/(.*)',
        headers: createSecureHeaders(),
      },
    ];
  },
};

module.exports = withPlugins([withBundleAnalyzer, withPWA, withImages, withSvgr], config);
