// const purgecss = [
//   '@fullhuman/postcss-purgecss',
//   {
//     content: ['./components/**/*.js', './pages/**/*.js'],
//     defaultExtractor: content => {
//       const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [];
//       const innerMatches = content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || [];
//       return broadMatches.concat(innerMatches);
//     },
//   },
// ];
const postcssPreset = [
  'postcss-preset-env',
  {
    autoprefixer: {
      flexbox: 'no-2009',
    },
    stage: 3,
    features: {
      'custom-properties': false,
    },
  },
];
module.exports = {
  plugins: [
    'postcss-import',
    'tailwindcss',
    'postcss-nested',
    'postcss-mixins',
    'autoprefixer',
    'postcss-flexbugs-fixes',
    postcssPreset,
  ],
};
