import { useContext, useRef, useState, createContext } from 'react';
import PropTypes from 'prop-types';
import AlertDialog from '@components/common/AlertDialog';

const AlertContext = createContext({
  catchOnCancel: null,
  variant: 'danger',
  title: '',
  description: '',
});
export const useAlertDialog = () => useContext(AlertContext);

export const AlertProvider = ({ children }) => {
  const [confirmationState, setConfirmationState] = useState(null);

  const awaitingConfirmationRef = useRef();
  const openConfirmation = options => {
    setConfirmationState(options);
    return new Promise((resolve, reject) => {
      awaitingConfirmationRef.current = { resolve, reject };
    });
  };
  const handleClose = () => {
    if (confirmationState?.catchOnCancel && awaitingConfirmationRef.current) {
      awaitingConfirmationRef.current.reject();
    }

    setConfirmationState(null);
  };
  const handleSubmit = () => {
    if (awaitingConfirmationRef.current) {
      awaitingConfirmationRef.current.resolve();
    }
    setConfirmationState(null);
  };

  return (
    <AlertContext.Provider value={openConfirmation}>
      {children}
      <AlertDialog
        open={Boolean(confirmationState)}
        onSubmit={handleSubmit}
        onClose={handleClose}
        {...confirmationState}
      />
    </AlertContext.Provider>
  );
};
AlertProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
