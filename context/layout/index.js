import { useState, createContext } from 'react';

const defaultState = {
  sidebarOpen: true,
  contentSidebar: null,
};

export const LayoutContext = createContext(defaultState);

export const LayoutProvider = props => {
  // eslint-disable-next-line react/prop-types
  const { children } = props;
  const [sidebarOpen, setSidebarOpen] = useState(true);
  const [sidebarContent, setSidebarContent] = useState(null);

  const dispatch = async action => {
    switch (action.type) {
      case 'SET_SIDEBAR_CONTENT':
        await setSidebarContent(action.content);
        await setSidebarOpen(true);
        break;
      case 'REMOVE_SIDEBAR_CONTENT':
        setSidebarContent(null);
        break;
      case 'TOGGLE_SIDEBAR':
        setSidebarOpen(prevState => !prevState);
        break;
      default:
        throw new Error();
    }
  };

  return (
    <LayoutContext.Provider
      value={{
        dispatch,
        sidebarOpen: sidebarOpen,
        sidebarContent: sidebarContent,
      }}
    >
      {children}
    </LayoutContext.Provider>
  );
};

export default LayoutContext;
