import { useState, createContext, useContext } from 'react';

const defaultState = {
  modalState: false,
  openModal: null,
  closeModal: null,
};

const ModalContext = createContext(defaultState);

export const useModal = () => useContext(ModalContext);

export const ModalProvider = props => {
  // eslint-disable-next-line react/prop-types
  const { children } = props;
  const [modalState, setModalState] = useState({
    open: false,
    content: '',
    title: '',
    icon: '',
  });

  const openModal = params => {
    setModalState({ ...params, open: true });
  };
  const closeModal = () => {
    setModalState({
      open: false,
      content: '',
      title: '',
      icon: '',
    });
  };

  return (
    <ModalContext.Provider
      value={{
        modalState: modalState,
        openModal: openModal,
        closeModal: closeModal,
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};

export default ModalContext;
