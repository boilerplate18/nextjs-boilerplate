import { UserProvider } from '@context/user';
import { LayoutProvider } from '@context/layout';
import { ModalProvider } from '@context/modal';

// eslint-disable-next-line react/prop-types
function ContextProvider({ children }) {
  return (
    <LayoutProvider>
      <UserProvider>
        <ModalProvider>{children}</ModalProvider>
      </UserProvider>
    </LayoutProvider>
  );
}
export default ContextProvider;
