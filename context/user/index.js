import { createContext } from 'react';
import { useSession } from 'next-auth/react';

import { useQuery } from 'react-query';
import createFetch from '@helpers/createFetch';
import { RESTAPI_HOST } from '@constants/index';
import { USERS } from '@constants/endpoint/query';
import replaceUrlId from '@helpers/replaceUrlId';

const defaultState = {
  authLoading: false,
  authError: null,
  userData: {},
};

export const UserContext = createContext(defaultState);

export const UserProvider = props => {
  // eslint-disable-next-line react/prop-types
  const { children } = props;

  const { data: session, status: statusSession } = useSession();
  console.log(session);
  const isAuthenticated = statusSession === 'authenticated';

  const token = (session && session?.token?.token) || '';

  const url = replaceUrlId(`${RESTAPI_HOST}${USERS.path}`, { userId: 1 });
  const { data, status: statusFetch, refetch } = useQuery({
    queryKey: [USERS.path],
    queryFn: createFetch(url, {
      authToken: token,
    }),
    enabled: isAuthenticated,
    cacheTime: 0,
  });
  const networkLoading = statusFetch === 'loading';

  const dispatch = action => {
    switch (action.type) {
      case 'REFETCH_MEMBER_DATA':
        refetch();

        break;

      default:
        throw new Error();
    }
  };

  return (
    <UserContext.Provider
      value={{
        dispatch,
        userData: data?.data,
        token: token,
        isLoggedIn: isAuthenticated,
        isLoading: networkLoading,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContext;
