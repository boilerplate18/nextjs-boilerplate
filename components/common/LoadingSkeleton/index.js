import React from 'react';
import { number, string, bool } from 'prop-types';

const LoadingSkeleton = props => {
  const { circle, className, count, height, width } = props;
  return (
    <div className={className}>
      {Array.from({ length: count ?? 1 }, (value, index) => {
        return (
          <div
            key={index}
            className="animate"
            style={{
              width,
              height,
              borderRadius: circle ? '50%' : height,
            }}
          />
        );
      })}
      <style jsx>{`
        .animate {
          display: flex;
          position: relative;
          margin: 1em 0;
          animation: shimmer 2s infinite;
          background: linear-gradient(to right, #dee0e2 4%, #d4d4d4 25%, #dddfe0 36%);
          background-size: 1000px 100%;
        }
        @keyframes shimmer {
          0% {
            background-position: -1000px 0;
          }
          100% {
            background-position: 1000px 0;
          }
        }
      `}</style>
    </div>
  );
};
LoadingSkeleton.defaultProps = {
  count: 1,
  className: '',
  width: '100%',
  height: '30px',
  circle: false,
};

LoadingSkeleton.propTypes = {
  circle: bool,
  className: string,
  count: number,
  height: string,
  width: string,
};
export default LoadingSkeleton;
