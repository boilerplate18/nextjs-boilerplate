import { useEffect, useRef, useState } from 'react';
import styles from './styles.module.css';

const PWAButton = () => {
  const deferredPrompt = useRef();
  const [installable, setInstallable] = useState(false);

  const beforeInstall = e => {
    // Prevent the mini-infobar from appearing on mobile
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt.current = e;
    // Update UI notify the user they can install the PWA
    setInstallable(true);
  };

  const appInstalled = () => {
    // Hide the app-provided install promotion
    setInstallable(false);
    // Clear the deferredPrompt so it can be garbage collected
    deferredPrompt.current = null;
    // Optionally, send analytics event to indicate successful install
    console.log('PWA was installed');
  };

  const onInstall = async () => {
    // Hide the app provided install promotion
    setInstallable(false);
    // Show the install prompt
    deferredPrompt?.current?.prompt();
    // Wait for the user to respond to the prompt
    const { outcome } = await deferredPrompt.userChoice;
    // Optionally, send analytics event with outcome of user choice
    console.log(`User response to the install prompt: ${outcome}`);
    // We've used the prompt, and can't use it again, throw it away
    deferredPrompt.current = null;
  };

  useEffect(() => {
    window.addEventListener('beforeinstallprompt', beforeInstall);
    window.addEventListener('appinstalled', appInstalled);
  }, []);

  return (
    <div className={`${styles.button} ${!installable && styles.hidden}`} onClick={onInstall}>
      Install
    </div>
  );
};
export default PWAButton;
