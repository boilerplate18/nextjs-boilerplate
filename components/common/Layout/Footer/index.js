import styles from './styles.module.css';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className="w-full px-4 py-8 flex justify-center">Footer</div>
    </footer>
  );
};
// test
export default Footer;
