import { styled } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: prop => prop !== 'open',
})(() => ({
  top: 0,
  left: 0,
  width: '50px',
  background: 'linear-gradient(-135deg, rgb(104, 186, 253) 0%, rgb(55, 132, 251) 100%)',
  height: '100vh',
  zIndex: '100',
  display: 'flex',
  alignItems: 'center',
  // ...(open && {
  //   width: `calc(100% - ${drawerWidth}px)`,
  //   marginLeft: `${drawerWidth}px`,
  //   transition: theme.transitions.create(['margin', 'width'], {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen,
  //   }),
  // }),
}));
export default AppBar;
