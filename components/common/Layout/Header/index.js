import { PureComponent } from 'react';
import { Button, Drawer } from '@mui/material';
import { Menu as MenuIcon } from '@mui/icons-material';

import Link from 'next/link';
import NavigationMenu from './NavigationMenu';
import styles from './styles.module.css';

class Header extends PureComponent {
  state = {
    open: false,
  };

  toggleDrawer = event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    this.setState(prevState => ({
      open: !prevState.open,
    }));
  };

  handleSearch = () => {
    console.log('search');
  };
  render() {
    const { open } = this.state;
    return (
      <header className={`${styles.header}`}>
        <nav className="w-full relative navbar-light justify-center flex flex-row p-4">
          <div className="container items-center justify-between flex flex-row">
            <Link href="/">
              <div className={styles.logo}>Logo</div>
            </Link>

            <div className="hidden lg:flex flex-1 ml-16 h-full justify-between">
              <NavigationMenu media="desktop" />
            </div>

            <Button className="flex lg:hidden" onClick={this.toggleDrawer} aria-label="Toggle Navigation">
              <MenuIcon />
            </Button>
            <Drawer anchor="right" open={open} onClose={this.toggleDrawer}>
              <NavigationMenu media="mobile" />
            </Drawer>
          </div>
        </nav>
      </header>
    );
  }
}
export default Header;
