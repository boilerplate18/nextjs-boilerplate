import { useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Button, Menu, MenuItem } from '@mui/material';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

import Link from 'next/link';

import styles from './styles.module.css';

const menus = [
  {
    path: '/',
    title: 'Beranda',
  },
  {
    path: '/menu-a',
    title: 'Menu A',
  },
  {
    path: '/menu-b',
    title: 'Menu B',
  },
  {
    path: '/menu-c',
    title: 'Menu C',
  },
];

const NavigationMenu = ({ media }) => {
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState(null);
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return router.pathname === url
      ? ` ${!hasSubmenu && 'menu-item-active'} ${media === 'desktop' ? styles.active : styles.activeMobile}`
      : '';
  };

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const renderPrivateMenu = () => {
    return (
      <div className="flex items-center justify-center">
        {media === 'desktop' ? (
          <>
            <AccountCircleOutlinedIcon
              className="text-4xl w-10 h-10 cursor-pointer"
              aria-controls="member-menu"
              aria-haspopup="true"
              onClick={handleClick}
            />
            <Menu
              id="member-menu"
              anchorEl={anchorEl}
              elevation={0}
              getContentAnchorEl={null}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
            >
              <MenuItem>Selamat datang, Test</MenuItem>
              <MenuItem className="flex flex-row space-x-2">
                <Link href="/profile/data-pribadi">
                  <Button variant="outlined" color="primary" onClick={handleClose} disableElevation className="w-1/2">
                    Profile
                  </Button>
                </Link>
                <Link href="/logout">
                  <Button variant="contained" color="primary" onClick={handleClose} disableElevation className="w-1/2">
                    Logout
                  </Button>
                </Link>
              </MenuItem>
            </Menu>
          </>
        ) : (
          <div className="flex flex-col mt-4">
            <div className="flex flex-row mx-4">
              <div className="text-sm font-medium">Selamat datang, Test</div>
            </div>
            <div className="flex mt-4 mx-4 space-x-4">
              <Link href="/profile/data-pribadi">
                <Button variant="outlined" color="primary" onClick={handleClose} disableElevation className="w-1/2">
                  Profile
                </Button>
              </Link>
              <Link href="/logout">
                <Button variant="contained" color="primary" onClick={handleClose} disableElevation className="w-1/2">
                  Logout
                </Button>
              </Link>
            </div>
          </div>
        )}
      </div>
    );
  };
  const renderPublicMenu = () => {
    return (
      <div className={`flex space-x-4 items-center ${media === 'mobile' && 'mt-4 mx-4'}`}>
        <Link href="/register">
          <Button variant="outlined" color="primary" disableElevation>
            Register
          </Button>
        </Link>
        <Link href="/login">
          <Button variant="contained" color="primary" disableElevation>
            Login
          </Button>
        </Link>
      </div>
    );
  };
  const renderCommonMenu = () => {
    return (
      <ul className={`${media === 'desktop' ? styles.navigationDesktop : styles.navigationMobile}`}>
        {menus.map(item => (
          <li className={getMenuItemActive(item.path)} key={item.path}>
            <Link href={item.path}>{item.title}</Link>
          </li>
        ))}
      </ul>
    );
  };

  return (
    <>
      {renderCommonMenu()}
      {false ? renderPrivateMenu() : renderPublicMenu()}
    </>
  );
};
NavigationMenu.propTypes = {
  media: PropTypes.string.isRequired,
};
export default NavigationMenu;
