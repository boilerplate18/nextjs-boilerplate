import { useContext, useEffect } from 'react';
import { useSession } from 'next-auth/react';

import { Divider, IconButton, Drawer, Button } from '@mui/material';
import {
  Close as CloseIcon,
  DateRange as DateRangeIcon,
  ArrowForwardIos as ArrowForwardIosIcon,
} from '@mui/icons-material';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { LayoutContext } from '@context/layout';
import Link from '@components/common/Link';
import AppBar from './AppBar';
import DrawerHeader from './DrawerHeader';
import Menus from './Menus';
import Main from './MainWrapper';

const drawerWidth = 310;

const Layout = ({ children }) => {
  const router = useRouter();

  const { sidebarOpen, dispatch } = useContext(LayoutContext);
  const toggleDrawer = event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    dispatch({ type: 'TOGGLE_SIDEBAR' });
  };

  const { data: session, status } = useSession();
  const isUser = !!session?.user;
  useEffect(() => {
    if (status === 'loading') return; // Do nothing while loading
    if (!isUser) router.push('/auth/login');
  }, [isUser, status, router]);

  // If no user, useEffect() will redirect.
  if (isUser) {
    return (
      <div className="bg-blue flex w-full justify-center flex-row">
        <div className="relative flex flex-row">
          <AppBar position="fixed" open={sidebarOpen}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              edge="start"
              className="mt-4 transofrm left-1/2 transform -translate-x-1/2"
              // sx={{ mr: 2 }}
            >
              {sidebarOpen ? (
                <div className="bg-primary-darkblue w-9 h-9 rounded-full flex items-center justify-center">
                  <CloseIcon />
                </div>
              ) : (
                <div className="bg-primary-darkblue w-9 h-9 rounded-full flex items-center justify-center">
                  <ArrowForwardIosIcon />
                </div>
              )}
            </IconButton>
          </AppBar>
          <Drawer
            sx={{
              width: drawerWidth,
              marginLeft: '50px',
              flexShrink: 0,
              position: 'relative',
              zIndex: '10',
            }}
            variant="persistent"
            anchor="left"
            open={sidebarOpen}
            elevation={16}
            hideBackdrop
            PaperProps={{
              sx: {
                position: 'absolute',
                height: '100vh',
                width: drawerWidth,
                boxSizing: 'border-box',
              },
            }}
          >
            <div className="flex flex-col h-screen">
              <DrawerHeader className="mx-4">Logo</DrawerHeader>
              <div
                style={{
                  background: 'linear-gradient(-135deg, rgb(104, 186, 253) 0%, rgb(55, 132, 251) 100%)',
                }}
                className="mt-4 rounded flex flex-col p-4 mx-4 relative"
              >
                <div className="text-white text-sm z-10">Today is Thursday</div>
                <div className="font-bold text-white text-base z-10">21 October 2021</div>
                <div className="absolute right-4 top-1/2 transform -translate-y-1/2 opacity-20 z-0">
                  <DateRangeIcon className="text-white text-5xl" />
                </div>
              </div>
              <Divider className="my-4" />
              <div className="flex flex-auto w-full">
                <Menus />
              </div>
              <div className="w-full p-4 flex self-end">
                <Link href="/auth/logout" className="w-full">
                  <Button type="button" color="primary" variant="contained" className=" rounded-2xl w-full font-bold">
                    Logout
                  </Button>
                </Link>
              </div>
            </div>
          </Drawer>
        </div>
        <Main open={sidebarOpen} width={drawerWidth}>
          {children}
        </Main>
      </div>
    );
  }
  return <div>Loading...</div>;
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
