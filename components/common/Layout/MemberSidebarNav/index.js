import { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { UserContext } from '@context/user';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useTheme, withStyles } from '@mui/material/styles';
import { Button, useMediaQuery } from '@mui/material';

import styles from './styles.module.css';

const StyledTabs = withStyles({
  indicator: {
    '&.MuiTabs-indicator': {
      display: 'none',
    },
  },
  flexContainer: {
    '&.MuiTabs-flexContainer': {},
  },
})(Tabs);

const StyledTab = withStyles({
  root: {
    '&': {
      maxWidth: 'none',
      display: 'flex',
      padding: '0',
    },
    '& .MuiTab-wrapper': {
      alignItems: 'flex-start',
      fontWeight: '300',
    },
  },
})(Tab);

const MemberSidebarNav = () => {
  const { userData } = useContext(UserContext);
  const privateRouteList = ['/ganti-password', '/data-pribadi', '/kuesioner', '/wishlist'];
  const classMenu =
    'text-base whitespace-nowrap text-gray-darkest py-1 pl-6 pr-6 lg:pr-12 text-center lg:text-left w-full';

  const router = useRouter();

  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    event?.preventDefault();
    setValue(newValue);
  };
  useEffect(() => {
    const activeTab = privateRouteList.findIndex(item => router.pathname.includes(item));
    setValue(activeTab);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.pathname]);

  const theme = useTheme();
  const isSmallViewPort = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <div className="flex flex-col w-full mb-8 lg:mb-0 md:flex-row lg:flex-col">
      <div className="bg-secondary-brown3 rounded-t pb-8 w-full">
        <div className="flex flex-col items-center mb-4 mr-0 md:mr-4 lg:mr-0 pt-4">
          <AccountCircleOutlinedIcon className="text-4xl w-24 h-24" />
          <p className="text-base font-medium text-center mt-3 mx-auto">Selamat datang,</p>
          <p className="text-base font-medium">{userData?.data?.nama}</p>
        </div>
        <StyledTabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          orientation={isSmallViewPort ? 'horizontal' : 'vertical'}
          variant="scrollable"
          scrollButtons="auto"
          className="w-full"
        >
          <StyledTab
            label={
              <Link href="/profile/data-pribadi">
                <div className={`${router.pathname.includes('/data-pribadi') && styles.active} ${classMenu}`}>
                  Data Pribadi
                </div>
              </Link>
            }
          />
          <StyledTab
            label={
              <Link href="/profile/kuesioner">
                <div className={`${router.pathname.includes('/kuesioner') && styles.active} ${classMenu}`}>
                  Kuesioner
                </div>
              </Link>
            }
          />
          <StyledTab
            label={
              <Link href="/profile/wishlist">
                <div className={`${router.pathname.includes('/wishlist') && styles.active} ${classMenu}`}>Wishlist</div>
              </Link>
            }
          />
          <StyledTab
            label={
              <Link href="/profile/ganti-password">
                <div className={`${router.pathname.includes('/ganti-password') && styles.active} ${classMenu}`}>
                  Ganti Password
                </div>
              </Link>
            }
          />
        </StyledTabs>
      </div>
      <Link href="/logout">
        <Button
          className="rounded-t-none rounded-b font-medium hidden lg:block"
          color="primary"
          disableElevation
          variant="contained"
        >
          Logout
        </Button>
      </Link>
    </div>
  );
};
export default MemberSidebarNav;
