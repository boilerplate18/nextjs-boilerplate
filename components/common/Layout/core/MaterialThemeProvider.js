import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { theme } from 'twin.macro';

export const muiTheme = createTheme(
  /**
   * @see https://material-ui.com/customization/themes/#theme-configuration-variables
   */
  {
    // direction: "rtl",
    typography: {
      fontFamily: theme`fontFamily.sans`,
    },
    breakpoints: {
      values: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
      },
    },
    palette: {
      primary: {
        // light: will be calculated from palette.primary.main,
        //  main: '#17c191',
        // dark: will be calculated from palette.primary.main,

        main: theme`colors.primary.darkblue`,
        contrastText: '#fff', //will be calculated to contrast with palette.primary.main
      },
      secondary: {
        // light: will be calculated from palette.primary.main,
        main: theme`colors.primary.yellow`,
        // dark: will be calculated from palette.primary.main,
        contrastText: '#fff', //will be calculated to contrast with palette.primary.main
      },
      error: {
        // light: will be calculated from palette.primary.main,
        main: theme`colors.secondary.red`,
        // dark: will be calculated from palette.primary.main,
        // contrastText: "#fff" //will be calculated to contrast with palette.primary.main
      },
      action: {
        main: '#17c191',
      },
      text: {
        disabled: 'rgba(0, 0, 0, 0.6)',
      },
    },

    components: {
      MuiAutocomplete: {
        styleOverrides: {
          popupIndicator: {
            backgroundColor: theme`colors.primary.darkblue`,
            width: '28px',
            height: '28px',
            color: 'white',
          },
          popupIndicatorOpen: {
            color: 'white',
          },
        },
      },
      MuiToolbar: {
        styleOverrides: {
          root: {
            paddingLeft: '0px',
            paddingRight: '0px',
          },
          gutter: {
            paddingLeft: '0px',
            paddingRight: '0px',
          },
        },
      },
      MuiTableCell: {
        styleOverrides: {
          root: {
            border: '1px solid rgba(224, 224, 224, 1)',
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            height: 'fit-content',
            textTransform: 'none',
            fontWeight: 400,
            fontFamily: theme`fontFamily.sans`,
            borderRadius: '5000px',
          },
        },
      },
      MuiInputLabel: {
        styleOverrides: {
          root: {
            fontSize: theme`fontSize.sm`,
            fontWeight: theme`fontWeight.medium`,
            color: theme`colors.primary.darkblue`,
          },
        },
      },
      MuiInputBase: {
        styleOverrides: {
          root: {
            borderRadius: '5000px',
          },
        },
      },
      MuiTablePagination: {
        styleOverrides: {
          root: {
            overflow: 'hidden',
          },
        },
      },
      MuiIconButton: {
        styleOverrides: {
          root: {
            padding: '0',
          },
        },
      },
    },
  },
);

export function MaterialThemeProvider(props) {
  const { children } = props;

  return <ThemeProvider theme={muiTheme}>{children}</ThemeProvider>;
}
MaterialThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
