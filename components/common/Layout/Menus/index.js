import { useState, Fragment } from 'react';
import { Collapse, List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import {
  Folder as FolderIcon,
  AccountCircle as AccountCircleIcon,
  ManageAccounts as ManageAccountsIcon,
  ExpandLess as ExpandLessIcon,
  ExpandMore as ExpandMoreIcon,
} from '@mui/icons-material';
import Link from '@components/common/Link';

const menus = [
  {
    key: 'user-management',
    name: 'User Management',
    icon: <FolderIcon className="text-secondary-blue" />,
    url: '',
    submenus: [
      {
        key: 'role',
        name: 'User Role',
        url: '/user-management/user-role',
      },
      {
        key: 'user',
        name: 'User',
        url: '/user-management/user',
      },
    ],
  },
  {
    key: 'user-management-admin',
    name: 'User Admin',
    icon: <AccountCircleIcon className="text-secondary-blue" />,
    url: '/user-management/user-admin',
  },
  {
    key: 'report',
    name: 'Report',
    icon: <FolderIcon className="text-secondary-blue" />,
    url: '',
    submenus: [
      {
        key: 'role',
        name: 'User Role',
        icon: <ManageAccountsIcon className="text-secondary-blue" />,
        url: '/master-data/user-role',
      },
      {
        key: 'user',
        name: 'User',
        icon: <AccountCircleIcon className="text-secondary-blue" />,
        url: '/master-data/user',
      },
    ],
  },
];

const Menus = () => {
  const [open, setOpen] = useState({
    master: false,
    report: false,
  });
  const handleClick = name => () => {
    setOpen(prev => ({ ...prev, [name]: !prev[name] }));
  };
  return (
    <List className="w-full">
      {menus.map(item => {
        if (Array.isArray(item?.submenus)) {
          return (
            <Fragment key={item.key}>
              <ListItemButton onClick={handleClick(item.key)}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText className="text-secondary-blue text-base" primary={item.name} />
                {open[item.key] ? (
                  <ExpandLessIcon className="text-secondary-blue" />
                ) : (
                  <ExpandMoreIcon className="text-secondary-blue" />
                )}
              </ListItemButton>
              <Collapse in={open[item.key]} timeout="auto" unmountOnExit>
                {item.submenus.map(submenu => {
                  return (
                    <Link href={submenu.url} key={submenu.key}>
                      <ListItemButton className="pl-20" button>
                        {submenu?.icon && <ListItemIcon>{submenu.icon}</ListItemIcon>}
                        <ListItemText className="text-secondary-blue text-base" primary={submenu.name} />
                      </ListItemButton>
                    </Link>
                  );
                })}
              </Collapse>
            </Fragment>
          );
        } else {
          return (
            <Link href={item.url} key={item.key}>
              <ListItemButton button key={item.key}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText className="text-secondary-blue text-base" primary={item.name} />
              </ListItemButton>
            </Link>
          );
        }
      })}
    </List>
  );
};
export default Menus;
