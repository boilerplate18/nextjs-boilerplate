import { Switch } from '@components/common/Inputs';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

const ControlledSwitch = ({ control, defaultValue, name, className, ...rest }) => {
  return (
    <Controller
      render={({ field: { onChange, value } }) => {
        const handleChange = (e, val) => {
          onChange(val);
        };
        return (
          <Switch onChange={handleChange} className={className} value={value} checked={Boolean(value)} {...rest} />
        );
      }}
      defaultValue={false}
      name={name}
      control={control}
    />
  );
};

ControlledSwitch.defaultProps = {
  className: '',
  defaultValue: null,
};

ControlledSwitch.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOf([null, PropTypes.object]),
  name: PropTypes.string.isRequired,
};

export default ControlledSwitch;
