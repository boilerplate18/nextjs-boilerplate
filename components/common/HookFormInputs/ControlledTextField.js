import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { TextField } from '@components/common/Inputs';

const ControlledTextField = ({
  control,
  defaultValue,
  name,
  className,
  label,
  onChangeEffect,
  placeholder,
  ...rest
}) => {
  return (
    <Controller
      render={({ field: { onChange, ...fieldProps } }) => {
        const handleChange = event => {
          typeof onChangeEffect === 'function' && onChangeEffect();
          onChange(event);
        };
        return (
          <TextField
            name={name}
            label={label}
            placeholder={placeholder}
            className={className}
            onChange={handleChange}
            {...fieldProps}
            {...rest}
          />
        );
      }}
      defaultValue={defaultValue}
      name={name}
      control={control}
    />
  );
};

ControlledTextField.defaultProps = {
  className: '',
  defaultValue: '',
  label: '',
  onChangeEffect: null,
  placeholder: '',
};

ControlledTextField.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChangeEffect: PropTypes.func,
};

export default ControlledTextField;
