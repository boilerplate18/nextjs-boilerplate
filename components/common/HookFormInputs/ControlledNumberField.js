import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import TextField from '@components/common/Inputs/TextField';

const ControlledSwitch = ({ control, defaultValue, prefix, suffix, format, name, ...rest }) => {
  return (
    <Controller
      render={({ field: { onChange, ...fieldProps } }) => {
        return (
          <NumberFormat
            allowEmptyFormatting={false}
            prefix={prefix}
            suffix={suffix}
            format={format}
            thousandSeparator
            {...fieldProps}
            customInput={TextField}
            onValueChange={c => {
              onChange(c?.value);
            }}
            {...rest}
          />
        );
      }}
      name={name}
      variant="outlined"
      defaultValue={defaultValue}
      control={control}
    />
  );
};

ControlledSwitch.defaultProps = {
  className: '',
  defaultValue: null,
  format: null,
  prefix: '',
  suffix: '',
};

ControlledSwitch.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOf([null, PropTypes.object]),
  format: PropTypes.string,
  name: PropTypes.string.isRequired,
  prefix: PropTypes.string,
  suffix: PropTypes.string,
};

export default ControlledSwitch;
