import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { SelectField } from '@components/common/Inputs';

const ControlledSelect = ({
  options,
  control,
  defaultValue,
  name,
  className,
  label,
  onChangeEffect,
  placeholder,
  ...rest
}) => {
  return (
    <Controller
      render={({ field: { onChange, ...fieldProps } }) => {
        const handleChange = event => {
          typeof onChangeEffect === 'function' && onChangeEffect();
          onChange(event);
        };
        return (
          <SelectField
            name={name}
            label={label}
            placeholder={placeholder}
            options={options}
            className={className}
            onChange={handleChange}
            {...fieldProps}
            {...rest}
          />
        );
      }}
      defaultValue={defaultValue}
      name={name}
      control={control}
    />
  );
};

ControlledSelect.defaultProps = {
  className: '',
  defaultValue: '',
  label: '',
  onChangeEffect: null,
  options: [],
  placeholder: '',
};

ControlledSelect.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.object,
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ),
  placeholder: PropTypes.string,
  onChangeEffect: PropTypes.func,
};

export default ControlledSelect;
