import { AutocompleteField } from '@components/common/Inputs';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';

const ControlledAutocompleteField = ({
  options,
  control,
  defaultValue,
  name,
  className,
  label,
  onChangeEffect,
  ...rest
}) => {
  return (
    <Controller
      render={({ field: { onChange, ...fieldProps } }) => {
        const handleChange = event => {
          const { value } = event;
          typeof onChangeEffect === 'function' && onChangeEffect();
          onChange(value);
        };
        return (
          <AutocompleteField
            options={options}
            onChange={handleChange}
            className={className}
            label={label}
            {...fieldProps}
            {...rest}
          />
        );
      }}
      defaultValue={defaultValue}
      name={name}
      control={control}
    />
  );
};

ControlledAutocompleteField.defaultProps = {
  className: '',
  defaultValue: null,
  label: '',
  onChangeEffect: null,
  options: [],
};

ControlledAutocompleteField.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOf([null, PropTypes.object]),
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.object,
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ),
  onChangeEffect: PropTypes.func,
};

export default ControlledAutocompleteField;
