import { Controller } from 'react-hook-form';
import { RadioGroup, FormControlLabel } from '@mui/material';
import PropTypes from 'prop-types';

import { Radio } from '@components/common/Inputs';

const ControlledRadio = ({ name, options, control, defaultValue, className }) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      render={({ field: { onChange, value, ref, name } }) => {
        const handleChange = e => {
          const { value: changedValue } = e.target;
          onChange(changedValue);
        };
        return (
          <RadioGroup row onChange={handleChange} value={value} name={name} className="w-full">
            {(options || []).map(item => {
              return (
                <FormControlLabel
                  className={className}
                  key={item.value}
                  value={item.value}
                  control={
                    <Radio
                      inputRef={ref}
                      icon={
                        <div className="border border-gray-800 w-full rounded text-sm px-3 py-2 break-all">
                          {item.label}
                        </div>
                      }
                      checkedIcon={
                        <div className="border border-secondary-blue w-full text-secondary-blue rounded px-3 text-sm py-2 break-all">
                          {item.label}
                        </div>
                      }
                    />
                  }
                />
              );
            })}
          </RadioGroup>
        );
      }}
    />
  );
};
ControlledRadio.defaultProps = {
  className: '',
  defaultValue: null,
};
ControlledRadio.propTypes = {
  className: PropTypes.string,
  control: PropTypes.object.isRequired,
  defaultValue: PropTypes.oneOfType(PropTypes.object, PropTypes.string, PropTypes.number),
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
};
export default ControlledRadio;
