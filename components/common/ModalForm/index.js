import { Backdrop, Modal, Fade, Box } from '@mui/material';

import { useModal } from '@context/modal';

const style = {
  position: 'relative',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
};

const ModalForm = () => {
  const { modalState, closeModal } = useModal();
  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={modalState.open}
        onClose={closeModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        keepMounted
      >
        <Fade in={modalState.open}>
          <Box sx={style}>
            <div
              className="-mx-2 bg-primary-darkblue w-full p-12 rounded relative flex justify-center items-center z-100"
              style={{ transform: 'scale(1.02)' }}
            >
              <div className="w-28 h-28 bg-primary-darkblue opacity-30 absolute transform -translate-y-10 rounded-full"></div>
              <div className="w-20 h-20 rounded-full bg-primary-darkblue absolute transform -translate-y-10 flex justify-center items-center">
                {modalState.icon}
              </div>
              <div className="text-white text-3xl font-semibold absolute top-1/2 transform -translate-y-1/2">
                {modalState.title}
              </div>
            </div>
            <div className="w-full relative flex flex-col items-center flex-auto">
              <div className="px-4 pt-8 pb-6 bg-white rounded-bl rounded-br w-full flex justify-center">
                {modalState.content}
              </div>
              <div className="bg-primary-darkblue absolute w-4/5 h-2 rounded-bl rounded-br bottom-0 transform translate-y-full"></div>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};
export default ModalForm;
