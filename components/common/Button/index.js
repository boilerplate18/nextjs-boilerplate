import { theme } from 'twin.macro';
import { Button } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledButton = withStyles({
  root: {
    background: 'rgb(26, 61, 114);',
    textTransform: 'none',
    fontFamily: theme`fontFamily.sans`,

    '&.MuiButton-outlined': {
      background: '#ffffff',
      borderColor: theme`colors.primary.darkblue`,
    },
    '&.MuiButton-outlined .MuiButton-label': {
      color: theme`colors.primary.darkblue`,
    },
  },
})(Button);
export default StyledButton;
