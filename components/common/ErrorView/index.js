export default function ErrorPage5() {
  return (
    <>
      <div className="flex flex-col flex-1">
        <div className="error-container">
          <div className="flex flex-col md:justify-center p-12">
            <h1 className="error-title font-bold text-primary-darkest mt-10 mb-12">Oops!</h1>
            <p className="font-bold text-4xl">Something went wrong here.</p>
            <p className="text-xl">
              We are working on it and we will get it fixed
              <br />
              as soon possible.
              <br />
              You can back or use our Help Center.
            </p>
          </div>
        </div>
      </div>
      <style jsx>{`
        .error-container {
          background-image: "url('/assets/error/bg5.jpg')"
          background-position: 50%;
          background-size: cover;
          display: flex;
          flex: 1 1 auto;
          height: 100vh;
        }
        .error-container h1 {
          font-size: 7rem !important;
        }
      `}</style>
    </>
  );
}
