import { Button, Dialog, DialogTitle, DialogContentText, DialogActions, DialogContent } from '@mui/material';
import PropTypes from 'prop-types';

const AlertDialog = ({ open, variant, title, description, onSubmit, onClose }) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      {title && <DialogTitle className="text-center px-0 py-4 bg-primary-darkblue text-white">{title}</DialogTitle>}
      <DialogContent className="my-4">
        <DialogContentText>{description}</DialogContentText>
      </DialogContent>
      <DialogActions className="flex flex-row justify-center space-x-4 mb-4">
        {variant === 'danger' && (
          <>
            <Button type="button" variant="contained" color="error" onClick={onClose}>
              Cancel
            </Button>
            <Button type="button" variant="contained" color="primary" onClick={onSubmit} autoFocus>
              Confirm
            </Button>
          </>
        )}
        {variant === 'info' && (
          <Button type="button" variant="contained" color="primary" onClick={onClose}>
            OK
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};
AlertDialog.defaultProps = {
  title: 'Confirmation',
  variant: 'danger',
  description: '',
};
AlertDialog.propTypes = {
  description: PropTypes.string,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  variant: PropTypes.oneOf(['danger', 'info']),
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
export default AlertDialog;
