import dynamic from 'next/dynamic';

const Datepicker = dynamic(() => import(/* webpackChunkName: "Datepickers" */ './Datepickers'), {
  ssr: false,
});
export { Datepicker };
