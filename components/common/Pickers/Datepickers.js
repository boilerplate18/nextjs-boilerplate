import PropTypes from 'prop-types';
import { DatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDayjs';

import { TextField } from '@components/common/Inputs';

const Datepicker = props => {
  const { className, name, label, error, helperText, ...rest } = props;

  return (
    <>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          name={name}
          className={className}
          error={error}
          {...rest}
          renderInput={params => <TextField label={label} {...params} error={error} helperText={helperText} />}
        />
      </LocalizationProvider>
    </>
  );
};

Datepicker.defaultProps = {
  className: null,
  error: false,
  format: 'DD-MM-YYYY',
  helperText: '',
};

Datepicker.propTypes = {
  className: PropTypes.string,
  error: PropTypes.bool,
  format: PropTypes.string,
  helperText: PropTypes.string,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default Datepicker;
