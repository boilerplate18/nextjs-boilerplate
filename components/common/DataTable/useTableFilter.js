import { useReducer } from 'react';

const actionTypes = {
  sort: 'SORT',
  search: 'SEARCH',
  page: 'PAGE',
  pageSize: 'PAGESIZE',
  clear: 'CLEAR',
  filter: 'FILTER',
};
const initialTableFilter = {
  search: '',
  sort: {
    columnName: '',
    sortOrder: 'ASC',
  },
  filter: {},
  page: 0,
  pageSize: 10,
};

function tableFilterReducer(state, action) {
  const { sort, search, pageSize, filter } = state;
  const { type, updatedState } = action;
  switch (type) {
    case actionTypes.filter: {
      const name = updatedState?.name || null;
      const value = updatedState?.value || null;
      if (!name) return state;
      return {
        search: search,
        filter: { ...filter, [name]: value },
        sort: sort,
        page: 0,
        pageSize: pageSize,
      };
    }
    case actionTypes.search: {
      if (updatedState instanceof String) return state;
      return {
        search: updatedState,
        sort: sort,
        filter: filter,
        page: 0,
        pageSize: pageSize,
      };
    }
    case actionTypes.sort: {
      const sortOrder = updatedState?.sortOrder || null;
      const columName = updatedState?.columnName || null;
      if (!sortOrder || !columName) return state;
      return {
        search: search,
        sort: {
          columnName: columName,
          sortOrder: sortOrder,
        },
        filter: filter,
        page: 0,
        pageSize: pageSize,
      };
    }
    case actionTypes.pageSize: {
      if (updatedState instanceof Number) return state;
      return {
        search: search,
        sort: sort,
        filter: filter,
        page: 0,
        pageSize: updatedState,
      };
    }
    case actionTypes.page: {
      if (updatedState instanceof Number) return state;
      return {
        search: search,
        sort: sort,
        filter: filter,
        page: updatedState,
        pageSize: pageSize,
      };
    }
    case actionTypes.clear: {
      return initialTableFilter;
    }
    default: {
      throw new Error(`Unhandled type: ${type}`);
    }
  }
}

function useTableFilter({ reducer = tableFilterReducer } = {}) {
  const [state, dispatch] = useReducer(reducer, initialTableFilter);
  const onSearch = (updatedState = '') => dispatch({ type: actionTypes.search, updatedState: updatedState });
  const onSort = (updatedState = initialTableFilter.sort) =>
    dispatch({ type: actionTypes.sort, updatedState: updatedState });
  const onFilter = (updatedState = initialTableFilter.filter) =>
    dispatch({ type: actionTypes.filter, updatedState: updatedState });
  const changePage = (updatedState = initialTableFilter.page) =>
    dispatch({ type: actionTypes.page, updatedState: updatedState });
  const changePageSize = (updatedState = initialTableFilter.pageSize) =>
    dispatch({ type: actionTypes.pageSize, updatedState: updatedState });
  const onClear = () => dispatch({ type: actionTypes.clear });
  return { state, onSearch, onSort, changePage, changePageSize, onFilter, onClear };
}

export { initialTableFilter, useTableFilter, tableFilterReducer, actionTypes };
