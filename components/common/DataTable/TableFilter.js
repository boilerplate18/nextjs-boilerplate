import React from 'react';
import { string, func, node, bool } from 'prop-types';
import { Button } from '@mui/material';
import { TextField } from '@components/common/Inputs';

import { styContainer, styFilter } from './styles.module.css';

const TableFilter = React.memo(
  ({ onSearch, placeholder, onClear, label, hideSearch, children, moreFilters, search, refetch }) => {
    const handleSearch = e => {
      const { value } = e.target;
      onSearch(value);
    };
    const handleClear = () => {
      onClear();
    };
    const handleRefetch = () => {
      refetch();
    };

    return (
      <div className={`flex flex-col ${styContainer}`}>
        <div className={`flex flex-row space-x-4 ${styFilter} w-full`}>
          {children ? children : moreFilters.map(node => node)}
          {!hideSearch && (
            <TextField
              placeholder={placeholder}
              onChange={handleSearch}
              name="search"
              label={label}
              value={search}
              size="small"
            />
          )}
        </div>
        <div className="flex flex-row items-center justify-end w-full mt-1">
          <Button type="button" variant="contained" color="primary" className="mr-4" onClick={handleRefetch}>
            Search
          </Button>
          <Button type="button" variant="outlined" color="primary" onClick={handleClear}>
            Clear
          </Button>
        </div>
      </div>
    );
  },
);
TableFilter.defaultProps = {
  children: '',
  search: '',
  placeholder: 'Search...',
  label: 'Search',
  moreFilters: [],
  hideSearch: false,
};
TableFilter.propTypes = {
  children: node,
  hideSearch: bool,
  label: string,
  moreFilters: node,
  placeholder: string,
  refetch: func.isRequired,
  search: string,
  onClear: func.isRequired,
  onSearch: func.isRequired,
};
export default TableFilter;
