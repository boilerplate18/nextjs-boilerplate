import React, { useCallback } from 'react';
import { useRouter } from 'next/router';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Tooltip,
  Checkbox,
  Button,
  IconButton,
} from '@mui/material';

import { withStyles } from '@mui/styles';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import { getObjectValue } from '@helpers/getObjectValue.js';

import EditableTableCell from './EditableTableCell';
import TableCellHeader from './TableCellHeader';
import { styRow, styTableBody, styFilterNone, styContainer } from './styles.module.css';

const WheatCheckbox = withStyles({
  root: {
    color: 'wheat',
    '&$checked': {
      color: 'wheat',
    },
  },
  checked: {},
})(props => <Checkbox color="default" {...props} />);

const StyledTableSort = withStyles(() => ({
  root: {
    '&.MuiTableSortLabel-active': {
      backgroundColor: 'rgb(2, 41, 100, 0.8);',
      color: 'white',
    },
    '&.MuiTableSortLabel-active .MuiTableSortLabel-icon': {
      color: 'white !important',
    },
  },
}))(TableSortLabel);

const DataTable = props => {
  const {
    className,
    headersGroup,
    headers,
    detailLink,
    updateLink,
    updateRow,
    deleteLink,
    deleteRow,
    rows,
    renderFunctions,
    sort,
    sortFunction,
    onCheckAll,
    Pagination,
    isFetching,
    onSubmitEdit,
  } = props;
  const router = useRouter();
  const hasActions = detailLink || updateLink || deleteLink || deleteRow;

  const renderActions = useCallback(
    row => {
      return (
        <div className="flex flex-row items-center space-x-4">
          {detailLink && (
            <IconButton
              onClick={() => router.push(`${typeof detailLink === 'object' ? detailLink.link : detailLink}${row?.id}`)}
            >
              <Tooltip title="Detail">{detailLink?.Icon ? detailLink.Icon : <VisibilityOutlinedIcon />}</Tooltip>
            </IconButton>
          )}
          {updateLink && (
            <Button
              type="button"
              variant="contained"
              color="secondary"
              className="flex items-center"
              disableElevation
              onClick={() => router.push(`${updateLink}${row?.id}`)}
            >
              Edit
            </Button>
          )}
          {updateRow && (
            <Button
              type="button"
              variant="contained"
              color="secondary"
              className="flex items-center"
              disableElevation
              onClick={() => updateRow(row)}
            >
              Edit
            </Button>
          )}
          {deleteLink && (
            <IconButton
              onClick={() => router.push(`${typeof deleteLink === 'object' ? deleteLink.link : deleteLink}${row?.id}`)}
            >
              <Tooltip title={deleteLink?.tooltip || 'Delete'}>
                {deleteLink?.Icon ? deleteLink.Icon : <DeleteIcon className="text-secondary-red" />}
              </Tooltip>
            </IconButton>
          )}
          {deleteRow && (
            <IconButton onClick={() => deleteRow(row)}>
              <Tooltip title="Delete">
                <DeleteIcon className="text-secondary-red" />
              </Tooltip>
            </IconButton>
          )}
        </div>
      );
    },
    [deleteLink, deleteRow, detailLink, router, updateLink, updateRow],
  );

  const renderCell = useCallback(
    (header, row, type) => {
      switch (type) {
        case 'text-editable':
          return (
            <EditableTableCell key={header.key} header={header} row={row} onSubmitEdit={onSubmitEdit}>
              {renderFunctions && renderFunctions[header.key]
                ? renderFunctions[header.key](row)
                : getObjectValue(row, header.key)}
            </EditableTableCell>
          );
        case 'text':
          return (
            <TableCell key={header.key}>
              {renderFunctions && renderFunctions[header.key]
                ? renderFunctions[header.key](row)
                : getObjectValue(row, header.key)}
            </TableCell>
          );
        case 'image':
          return (
            <TableCell key={header.key}>
              <img
                src={getObjectValue(row, header.key)}
                style={{ maxWidth: '150px', height: 'auto', objectFit: 'contain' }}
                alt="Empty"
              />
            </TableCell>
          );
        default:
          return (
            <TableCell key={header.key}>
              {renderFunctions && renderFunctions[header.key]
                ? renderFunctions[header.key](row)
                : getObjectValue(row, header.key)}
            </TableCell>
          );
      }
    },
    [onSubmitEdit, renderFunctions],
  );

  return (
    <div className={`${className} ${styContainer}`}>
      <PerfectScrollbar
        containerRef={ref => {
          if (ref) {
            // https://github.com/mdbootstrap/perfect-scrollbar/pull/934/files
            ref._getBoundingClientRect = ref.getBoundingClientRect;

            ref.getBoundingClientRect = () => {
              const original = ref._getBoundingClientRect();

              return {
                ...original,
                width: Math.floor(original.width),
                height: Math.floor(original.height),
              };
            };
          }
        }}
        options={{
          wheelSpeed: 2,
          wheelPropagation: false,
        }}
        style={{ position: 'relative', height: '450px' }}
      >
        <div>
          <Table size="medium" aria-label="a sticky table" stickyHeader>
            <TableHead>
              {headersGroup.length > 0 && (
                <TableRow>
                  {headersGroup.map(headerGroup => (
                    <TableCellHeader
                      key={headerGroup.key}
                      colSpan={headerGroup.colSpan}
                      className={`${!headerGroup.groupName ? styFilterNone : ''}`}
                    >
                      {headerGroup.groupName}
                    </TableCellHeader>
                  ))}
                  {hasActions && <TableCellHeader />}
                </TableRow>
              )}
              <TableRow>
                {headers.map(header => {
                  const isCheckedKey = header.key === 'checked' && onCheckAll;
                  return isCheckedKey ? (
                    <TableCellHeader key={header.key}>
                      <WheatCheckbox name="checkAll" onChange={onCheckAll} />
                    </TableCellHeader>
                  ) : (
                    <TableCellHeader key={header.key}>
                      {(header?.sortable === undefined || header.sortable) && (
                        <StyledTableSort
                          active={sort.columnName === header.key}
                          direction={sort.sortOrder.toLowerCase()}
                          onClick={() => sortFunction(header.key, sort.sortOrder === 'ASC' ? 'DESC' : 'ASC')}
                        >
                          {header.displayName}
                        </StyledTableSort>
                      )}
                      {header.sortable !== undefined && !header.sortable && <>{header.displayName} </>}
                    </TableCellHeader>
                  );
                })}
                {hasActions && <TableCellHeader>Actions</TableCellHeader>}
              </TableRow>
            </TableHead>
            <TableBody className={styTableBody}>
              {!isFetching &&
                rows.map(row => (
                  <TableRow className={styRow} hover key={row?.key ? row.key : row?.id}>
                    {headers.map(header => {
                      const type = header?.type !== 'image' ? (header?.editable ? 'text-editable' : 'text') : 'image';
                      return renderCell(header, row, type);
                    })}
                    {hasActions && <TableCell>{renderActions(row)}</TableCell>}
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          {!isFetching && rows.length === 0 && (
            <div className="flex items-center justify-center my-4">Data Tidak Ditemukan</div>
          )}
        </div>
      </PerfectScrollbar>
      {rows.length > 0 && Pagination}
    </div>
  );
};

DataTable.defaultProps = {
  className: null,
  deleteLink: '',
  deleteRow: null,
  detailLink: '',
  headersGroup: [],
  isFetching: false,
  rows: [],
  renderFunctions: {},
  sort: {
    sortOrder: 'DESC',
    columnName: 'id',
  },
  sortFunction: null,
  onCheckAll: null,
  onSubmitEdit: null,
  Pagination: null,
  updateLink: '',
  updateRow: null,
};

DataTable.propTypes = {
  className: PropTypes.string,
  deleteLink: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      Icon: PropTypes.node,
      link: PropTypes.string,
      tooltip: PropTypes.string,
    }),
  ]),
  deleteRow: PropTypes.func,
  detailLink: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      Icon: PropTypes.node,
      link: PropTypes.string,
      tooltip: PropTypes.string,
    }),
  ]),
  headers: PropTypes.arrayOf(PropTypes.object).isRequired,
  headersGroup: PropTypes.arrayOf(PropTypes.object),
  isFetching: PropTypes.bool,
  Pagination: PropTypes.node,
  renderFunctions: PropTypes.object,
  rows: PropTypes.arrayOf(PropTypes.object),
  sort: PropTypes.shape({
    columnName: PropTypes.string,
    sortOrder: PropTypes.string,
  }),
  sortFunction: PropTypes.func,
  updateLink: PropTypes.string,
  updateRow: PropTypes.func,
  onCheckAll: PropTypes.func,
  onSubmitEdit: PropTypes.func,
};

export default React.memo(DataTable);
