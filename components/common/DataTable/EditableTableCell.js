import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { TableCell } from '@mui/material';
import { TextField } from '@components/common/Inputs';
import useOuterClick from '@hooks/useOuterClick';
import { styFocus } from './styles.module.css';

const EditableTableCell = React.memo(({ children, onSubmitEdit, row, header }) => {
  const [editing, setEditing] = useState(false);
  const [cellFocused, setCellFocused] = useState(false);

  const inputRef = useRef('');
  const wrapperRef = useOuterClick(e => {
    e.stopPropagation();
    setEditing(false);
  });
  const tableCellRef = useOuterClick(e => {
    e.stopPropagation();
    setCellFocused(false);
  });

  const toggleEditing = () => {
    setEditing(prevState => !prevState);
  };
  const onSubmitEditing = () => {
    onSubmitEdit(row, header.key, inputRef.current.value);
  };
  const toggleFocus = e => {
    e.stopPropagation();
    setCellFocused(true);
  };
  return (
    <TableCell onDoubleClick={toggleEditing} onClick={toggleFocus} ref={tableCellRef}>
      {!editing ? (
        <div className={`${cellFocused ? styFocus : ''} flex flex-col`}>{children}</div>
      ) : (
        <div ref={wrapperRef} className="flex flex-col">
          <TextField inputRef={inputRef} className="mb-3" name={header.key} label={header.displayName} autoFocus />
          <button type="button" className="btn btn-warning btn-sm" onClick={onSubmitEditing}>
            Submit
          </button>
        </div>
      )}
    </TableCell>
  );
});
EditableTableCell.propTypes = {
  children: PropTypes.string.isRequired,
  header: PropTypes.object.isRequired,
  row: PropTypes.object.isRequired,
  onSubmitEdit: PropTypes.func.isRequired,
};
export default EditableTableCell;
