import { TableCell } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledTableCell = withStyles(() => ({
  head: {
    '&:focus': {
      outline: 'none !important',
    },
    backgroundColor: 'white',
    color: '#022964',
    whiteSpace: 'nowrap',
    '& .MuiButtonBase-root': {
      whiteSpace: 'nowrap',
    },
    '&:hover .MuiButtonBase-root': {
      color: '#022964',
      opacity: '0.75',
    },
  },
}))(TableCell);
export default StyledTableCell;
