import dynamic from 'next/dynamic';

const TableCellHeader = dynamic(() => import(/* webpackChunkName: "TableCellHeader" */ './TableCellHeader'), {
  ssr: false,
});
const DataTable = dynamic(() => import(/* webpackChunkName: "DataTable" */ './DataTable'), {
  ssr: false,
});
const TableFilter = dynamic(() => import(/* webpackChunkName: "TableFilter" */ './TableFilter'), {
  ssr: false,
});

export { DataTable, TableFilter, TableCellHeader };
