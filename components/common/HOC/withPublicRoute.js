import { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import { UserContext } from '@context/user';
import hoistNonReactStatics from 'hoist-non-react-statics';

function withPublicRoute(Component) {
  return hoistNonReactStatics(props => {
    const { isLoggedIn } = useContext(UserContext);
    const router = useRouter();

    useEffect(() => {
      if (isLoggedIn) {
        router.replace(router.query?.ld || '/');
      }
    }, [isLoggedIn, router]);
    return <Component {...props} />;
  }, Component);
}

export default withPublicRoute;
