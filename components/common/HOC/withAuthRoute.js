import { useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import { UserContext } from '@context/user';
import hoistNonReactStatics from 'hoist-non-react-statics';

function withAuthRoute(Component) {
  return hoistNonReactStatics(props => {
    const { isLoggedIn } = useContext(UserContext);
    const router = useRouter();

    useEffect(() => {
      const prevLink = router.pathname !== '/logout' ? router.pathname : '';
      if (!isLoggedIn) {
        router.replace(`/login?ld=${prevLink}`);
      }
    }, [router, isLoggedIn]);

    return <Component {...props} />;
  }, Component);
}

export default withAuthRoute;
