import { theme } from 'twin.macro';
import { Checkbox } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledCheckbox = withStyles({
  root: {
    color: theme`colors.primary.darkblue`,
  },
})(Checkbox);

export default StyledCheckbox;
