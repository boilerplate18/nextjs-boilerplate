import dynamic from 'next/dynamic';
import NumberFormat from './NumberFormat';

const TextField = dynamic(() => import('./TextField'));
const Checkbox = dynamic(() => import('./Checkbox'));
const Radio = dynamic(() => import('./Radio'));
const Switch = dynamic(() => import('./Switch'));
const AutocompleteField = dynamic(() => import('./AutocompleteField'));
const SelectField = dynamic(() => import('./SelectField'));

export { TextField, Checkbox, Radio, AutocompleteField, Switch, NumberFormat, SelectField };
