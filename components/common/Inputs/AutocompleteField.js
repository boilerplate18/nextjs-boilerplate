import PropTypes from 'prop-types';
import { Autocomplete } from '@mui/material';
import TextField from './TextField';

const AutocompleteField = ({
  className,
  options,
  label,
  inputParams,
  onChange,
  name,
  error,
  helperText,
  placeholder,
  defaultValue,
  ...rest
}) => {
  const handleChange = (e, value) => {
    const event = {
      name: name,
      value: value,
    };
    onChange(event);
  };
  return (
    <Autocomplete
      className={className}
      options={options}
      isOptionEqualToValue={(option, value) => {
        return String(option?.value) === String(value?.value);
      }}
      getOptionLabel={option => option?.label}
      renderInput={params => (
        <TextField
          {...params}
          name={name}
          label={label}
          variant="outlined"
          size="small"
          {...inputParams}
          error={error}
          helperText={helperText}
          placeholder={placeholder}
        />
      )}
      onChange={handleChange}
      {...rest}
    />
  );
};
AutocompleteField.defaultProps = {
  className: '',
  defaultValue: {},
  error: false,
  helperText: '',
  inputParams: {},
  options: [],
  placeholder: '',
};
AutocompleteField.propTypes = {
  className: PropTypes.string,
  defaultValue: PropTypes.object,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  inputParams: PropTypes.object,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.object,
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ),
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};
export default AutocompleteField;
