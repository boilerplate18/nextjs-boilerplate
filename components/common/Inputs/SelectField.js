import PropTypes from 'prop-types';
import { Select, FormControl, FormHelperText, MenuItem } from '@mui/material';
import { theme } from 'twin.macro';
import { withStyles } from '@mui/styles';

const StyledSelect = withStyles({
  root: {
    '&': {
      minHeight: '40px',
      borderRadius: '5000px',
      borderColor: theme`colors.primary.darkblue`,
    },
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: theme`colors.primary.darkblue`,
    },
  },
})(Select);

const SelectField = ({
  className,
  options,
  label,
  inputParams,
  onChange,
  name,
  error,
  helperText,
  placeholder,
  defaultValue,
  ...rest
}) => {
  return (
    <FormControl variant="outlined" size="small" className={className} error={error}>
      <StyledSelect
        onChange={onChange}
        inputProps={{
          name: name,
        }}
        displayEmpty
        {...rest}
      >
        <MenuItem value="">{placeholder}</MenuItem>
        {options.map(item => {
          return (
            <MenuItem key={item?.value} value={item?.value}>
              {item?.label}
            </MenuItem>
          );
        })}
      </StyledSelect>
      {error && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};
SelectField.defaultProps = {
  className: '',
  defaultValue: {},
  error: false,
  helperText: '',
  inputParams: {},
  options: [],
  placeholder: '',
};
SelectField.propTypes = {
  className: PropTypes.string,
  defaultValue: PropTypes.object,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  inputParams: PropTypes.object,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      data: PropTypes.object,
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    }),
  ),
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};
export default SelectField;
