import { theme } from 'twin.macro';
import { Radio } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledRadio = withStyles({
  root: {
    '& .MuiRadio-root': {
      color: theme`colors.primary.darkblue`,
      flex: '1 1 auto',
    },
  },
})(Radio);

export default StyledRadio;
