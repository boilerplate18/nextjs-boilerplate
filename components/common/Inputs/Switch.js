import { theme } from 'twin.macro';
import { Switch } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledSwitch = withStyles(muiTheme => ({
  root: {
    width: 80,
    height: 48,
    padding: 8,
    marginLeft: '-8px',
  },
  switchBase: {
    padding: 11,
    color: theme`colors.primary.darkblue`,
    '&:hover': {
      background: 'inherit',
    },
  },
  thumb: {
    width: 26,
    height: 26,
    backgroundColor: '#fff',
  },
  track: {
    transition: muiTheme.transitions.create(['background-color', 'border']),
    background: '#d8d8d8',
    opacity: '1 !important',
    borderRadius: 20,
    position: 'relative',
    '&:before, &:after': {
      display: 'inline-block',
      position: 'absolute',
      top: '50%',
      width: '50%',
      transform: 'translateY(-50%)',
      color: '#fff',
      textAlign: 'center',
    },
    '&:before': {
      content: '"L"',
      fontSize: '24px',
      fontWeight: '400',
      transform: 'scaleX(-1) rotate(-45deg)',
      left: 4,
      top: -2,
      opacity: 0,
    },
    '&:after': {
      content: '""',
      top: 0,
      right: 4,
    },
  },
  checked: {
    '&$switchBase': {
      color: 'white',
      transform: 'translateX(32px)',
    },
    '& $thumb': {
      backgroundColor: '#fff',
    },
    '& + $track': {
      background: theme`colors.primary.darkblue`,
      '&:before': {
        opacity: 1,
      },
      '&:after': {
        opacity: 0,
      },
    },
  },
}))(Switch);

export default StyledSwitch;
