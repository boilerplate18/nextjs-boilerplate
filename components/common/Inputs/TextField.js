import { theme } from 'twin.macro';
import { TextField } from '@mui/material';
import { withStyles } from '@mui/styles';

const StyledTextField = withStyles({
  root: {
    '&': {
      minHeight: '40px',
    },
    '& input': {
      fontSize: theme`fontSize.sm`,
      minHeight: '19px',
      color: theme`colors.primary.darkblue`,
    },
    '& label': {
      fontSize: theme`fontSize.xs`,
    },
    '& label.Mui-focused': {
      color: theme`colors.primary.darkblue`,
      marginLeft: '4px',
    },
    '& .MuiOutlinedInput-root': {
      borderRadius: '5000px',
      '& fieldset': {
        top: '-4.5px',
        borderColor: theme`colors.primary.darkblue`,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme`colors.primary.darkblue`,
      },
    },
  },
})(TextField);

export default StyledTextField;
