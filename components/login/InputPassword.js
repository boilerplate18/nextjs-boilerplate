/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/sort-prop-types */
/* eslint-disable react/prop-types */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ErrorText from './ErrorText';

const eyeHide = '/assets/icons/eye-hide.svg';
const eye = '/assets/icons/eye.svg';

export default function InputPassword(props) {
  const [hidePassword, setHidePassword] = useState(true);

  const handleHide = () => {
    setHidePassword(!hidePassword);
  };

  return (
    <div className={`flex flex-col justify-start mt-4 lg:mt-5 ${props.tWidth ? props.tWidth : 'w-full lg:w-6/12'}`}>
      <label htmlFor={props.htmlFor} className="flex items-start text-primary-darkblue text-sm mb-2 select-none">
        {props.label}
      </label>

      <div
        className={`from-input flex flex-row items-center justify-start px-4 py-3 rounded-full  text-sm border-2 transition-all duration-150 focus:ring-1 focus:outline-none ${
          props.error ? 'border-secondary-red focus:border-secondary-red' : 'border-gray-200 focus:border-gray-200'
        } ${props.disabled ? 'bg-gray-100 cursor-not-allowed' : 'bg-white'}`}
      >
        <img src={props.icon} className="h-5 w-5" alt={props.altIcon} />
        <input
          className="ml-3 w-full focus:outline-none"
          onFocus={props.onFocus}
          onInput={props.onInput}
          onChange={props.onChange}
          defaultValue={props.defaultValue}
          value={props.value}
          ref={props.register}
          type={hidePassword ? 'password' : 'text'}
          placeholder={props.placeholder}
          {...props.register(props.htmlFor, props.rules)}
          disabled={props.disabled}
        />

        <img
          src={hidePassword ? eye : eyeHide}
          onClick={handleHide}
          className="h-5 w-5 cursor-pointer"
          alt={props.altIcon}
        />
      </div>

      {props.error && <ErrorText>{props.error.message}</ErrorText>}
    </div>
  );
}

InputPassword.propTypes = {
  defaultValue: PropTypes.string,
  error: PropTypes.object.isRequired,
  htmlFor: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onfocus: PropTypes.func,
  onInput: PropTypes.func,
  onChange: PropTypes.func,
  register: PropTypes.func,
  rules: PropTypes.any,
  tel: PropTypes.bool,
  tWidth: PropTypes.string,
  type: PropTypes.string,
};

InputPassword.defaultProps = {
  defaultValue: '',
  htmlFor: '',
  label: '',
  placeholder: '',
  rules: {},
  tel: false,
  tWidth: 'w-full lg:w-6/12',
  type: 'text',
};
