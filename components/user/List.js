import { Card, TablePagination } from '@mui/material';
import DriveEtaIcon from '@mui/icons-material/DriveEta';

import { AutocompleteField } from '@components/common/Inputs';
import { useModal } from '@context/modal';
import UserForm from '@components/user/Form';
import { useAlertDialog } from '@context/alert';

/* Data Table Dependency */
import LoadingSkeleton from '@components/common/LoadingSkeleton';
import { useTableFilter } from '@components/common/DataTable/useTableFilter';
import { DataTable, TableFilter } from '@components/common/DataTable';

const List = () => {
  const confirm = useAlertDialog();
  const { openModal } = useModal();
  const { state: filterState, onSearch, onSort, changePage, changePageSize, onClear, onFilter } = useTableFilter();
  const { search, sort, page, pageSize, filter } = filterState;

  const sortFunction = (columnName, sortOrder) => {
    onSort({
      columnName,
      sortOrder,
    });
  };

  const handleChangePage = (event, newPage) => {
    changePage(newPage);
  };
  const handleChangePageSize = event => {
    changePageSize(parseInt(event.target.value, 10));
  };

  /* More Filter */
  const moreFilters = [
    <AutocompleteField
      options={[
        { label: 'test1', value: 1 },
        { label: 'test2', value: 2 },
      ]}
      key={1}
      placeholder="Example More"
      onChange={e => onFilter({ name: e.name, value: e.value })}
      name="morefilter"
      label="Example More"
      value={filter?.morefilter || null}
      className="w-100"
    />,
  ];

  /* Fetch Data */

  /* End Fetch Data */
  const tableList = [
    {
      id: 1,
      key_one: 'Test',
      key_two: 'Dummy',
      key_three: 'Three',
    },
    {
      id: 2,
      key_one: 'Test',
      key_two: 'Dummy',
      key_three: 'Three',
    },
    {
      id: 3,
      key_one: 'Test',
      key_two: 'Dummy',
      key_three: 'Three',
    },
    {
      id: 4,
      key_one: 'Test',
      key_two: 'Dummy',
      key_three: 'Three',
    },
    {
      id: 5,
      key_one: 'Test',
      key_two: 'Dummy',
      key_three: 'Three',
    },
  ];
  const pagination = {
    total: 3,
  };

  const updateRow = row => {
    openModal({
      title: 'Edit User',
      content: <UserForm selectedRow={row} />,
      icon: <DriveEtaIcon className="text-white" />,
    });
  };
  const deleteRow = row => {
    confirm({
      catchOnCancel: false,
      description: `Are you sure you want to delete ID#${row?.id}`,
    }).then(() => {
      console.log('hit mutation');
    });
  };
  return (
    <>
      <Card className="p-4 mb-4">
        <TableFilter
          search={search}
          onSearch={onSearch}
          placeholder="Search..."
          label="Search"
          onClear={onClear}
          moreFilters={moreFilters}
          refetch={() => console.log('call refetch func')}
        />
      </Card>
      <Card className="py-4">
        {false ? (
          <LoadingSkeleton className="px-4" count={3} />
        ) : (
          <>
            <DataTable
              rows={tableList}
              isFetching={false}
              // updateLink="/manage-student/student/update/"
              updateRow={updateRow}
              deleteRow={deleteRow}
              headers={[
                { displayName: 'Col 1', key: 'key_one' },
                { displayName: 'Col 2', key: 'key_two' },
                { displayName: 'Col 3', key: 'key_three' },
              ]}
              renderFunctions={{
                key_one: row => {
                  return `Example ${row.key_one}`;
                },
              }}
              sort={sort}
              sortFunction={sortFunction}
              Pagination={
                <TablePagination
                  component="div"
                  count={pagination?.total}
                  page={page}
                  rowsPerPageOptions={[10, 25]}
                  onPageChange={handleChangePage}
                  rowsPerPage={pageSize}
                  onRowsPerPageChange={handleChangePageSize}
                />
              }
            />
          </>
        )}
      </Card>
    </>
  );
};
export default List;
