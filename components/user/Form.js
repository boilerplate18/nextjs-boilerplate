import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Button, InputLabel } from '@mui/material';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import get from 'lodash/get';

import { useModal } from '@context/modal';
import { phoneNumberRegex } from '@constants/regex';
import useYupValidationResolver from '@hooks/useYupValidationResolver';
import { ControlledSelect, ControlledTextField } from '@components/common/HookFormInputs';

const UserForm = ({ selectedRow }) => {
  const { closeModal } = useModal();
  const classes = {
    inputField: 'flex flex-col w-full',
  };
  console.log('Edit the:', selectedRow);

  const validationSchema = useMemo(
    () =>
      Yup.object({
        email: Yup.string()
          .email('Use valid email!')
          .required('Required!'),
        name: Yup.string()
          .required('Required!')
          .matches(/^[a-zA-Z.]+$/, 'Must contain alphabet only!'),
        phone_number: Yup.string().test('regex', 'Must contain numeric only dan minimum 6 char!', val => {
          let regExp = new RegExp(phoneNumberRegex);
          return regExp.test(val);
        }),
        role_id: Yup.string().required('Required!'),
      }),
    [],
  );
  const resolver = useYupValidationResolver(validationSchema);

  const { handleSubmit, formState, control } = useForm({
    reValidateMode: 'onSubmit',
    defaultValues: {},
    resolver,
  });
  const { errors, isSubmitting } = formState;

  const runSubmit = async params => {
    try {
      console.log('hit api call', { params });
    } catch (error) {
      return;
    }
  };
  return (
    <form className="space-y-4 w-full" onSubmit={handleSubmit(runSubmit)}>
      <div className={classes.inputField}>
        <InputLabel shrink htmlFor="name" required error={!!errors?.name}>
          Name
        </InputLabel>
        <ControlledTextField
          size="small"
          id="name"
          className="w-full"
          placeholder="Name"
          label=""
          variant="outlined"
          name="name"
          control={control}
          helperText={get(errors, ['name', 'message'])}
          error={!!errors?.name}
        />
      </div>
      <div className={classes.inputField}>
        <InputLabel shrink htmlFor="email" required error={!!errors?.email}>
          Email
        </InputLabel>
        <ControlledTextField
          size="small"
          id="email"
          className="w-full"
          placeholder="Email"
          label=""
          variant="outlined"
          name="email"
          control={control}
          helperText={get(errors, ['email', 'message'])}
          error={!!errors?.email}
        />
      </div>
      <div className={classes.inputField}>
        <InputLabel shrink htmlFor="phone_number" error={!!errors?.phone_number}>
          Phone Number
        </InputLabel>
        <ControlledTextField
          size="small"
          id="phone_number"
          className="w-full"
          placeholder="Phone Number"
          label=""
          variant="outlined"
          name="phone_number"
          control={control}
          helperText={get(errors, ['phone_number', 'message'])}
          error={!!errors?.phone_number}
        />
      </div>
      <div className={classes.inputField}>
        <InputLabel shrink htmlFor="role_id" error={!!errors?.role_id} required>
          Role
        </InputLabel>
        <ControlledSelect
          className="w-full"
          options={[
            { label: 'test1', value: 1 },
            { label: 'test2', value: 2 },
          ]}
          name="role_id"
          placeholder="Select Role"
          label=""
          helperText={get(errors, ['role_id', 'message'])}
          error={!!errors?.role_id}
          control={control}
        />
      </div>
      <div className="flex flex-row space-x-4">
        <Button color="error" variant="contained" type="button" className="w-1/2 font-medium" onClick={closeModal}>
          Cancel
        </Button>
        <Button color="primary" variant="contained" type="submit" className="w-1/2 font-medium" disabled={isSubmitting}>
          {isSubmitting ? '...' : 'Save'}
        </Button>
      </div>
    </form>
  );
};
UserForm.defaultProps = {
  selectedRow: null,
};
UserForm.propTypes = {
  selectedRow: PropTypes.object,
};
export default UserForm;
