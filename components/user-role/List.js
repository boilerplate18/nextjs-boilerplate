import { Card, TablePagination, Button, Tooltip, IconButton } from '@mui/material';
import { DriveEta as DriveEtaIcon, Delete as DeleteIcon, Settings as SettingsIcon } from '@mui/icons-material';

import { useModal } from '@context/modal';
import RoleForm from '@components/user-role/Form';
import { useAlertDialog } from '@context/alert';
import Link from '@components/common/Link';

/* Data Table Dependency */
import LoadingSkeleton from '@components/common/LoadingSkeleton';
import { useTableFilter } from '@components/common/DataTable/useTableFilter';
import { DataTable, TableFilter } from '@components/common/DataTable';

const List = () => {
  const { openModal } = useModal();
  const confirm = useAlertDialog();

  const { state: filterState, onSearch, onSort, changePage, changePageSize, onClear } = useTableFilter();
  const { search, sort, page, pageSize } = filterState;

  const sortFunction = (columnName, sortOrder) => {
    onSort({
      columnName,
      sortOrder,
    });
  };

  const handleChangePage = (event, newPage) => {
    changePage(newPage);
  };
  const handleChangePageSize = event => {
    changePageSize(parseInt(event.target.value, 10));
  };

  /* Fetch Data */

  /* End Fetch Data */
  const tableList = [
    {
      id: 1,
      role_name: 'Test 1',
    },
    {
      id: 2,
      role_name: 'Test 2',
    },
  ];
  const pagination = {
    total: 3,
  };

  const updateRow = row => {
    openModal({
      title: 'Edit User Role',
      content: <RoleForm selectedRow={row} />,
      icon: <DriveEtaIcon className="text-white" />,
    });
  };
  const deleteRow = row => {
    confirm({
      catchOnCancel: false,
      description: `Are you sure you want to remove ID#${row?.id}`,
    }).then(() => {
      console.log('call mutation services');
    });
  };
  return (
    <>
      <Card className="p-4 mb-4">
        <TableFilter
          search={search}
          onSearch={onSearch}
          placeholder="Search..."
          label="Search"
          onClear={onClear}
          refetch={() => console.log('call refetch func')}
        />
      </Card>
      <Card className="py-4">
        {false ? (
          <LoadingSkeleton className="px-4" count={3} />
        ) : (
          <>
            <DataTable
              rows={tableList}
              isFetching={false}
              headers={[
                { displayName: 'Role Name', key: 'role_name' },
                { displayName: 'Actions', key: 'custom_actions' },
              ]}
              renderFunctions={{
                custom_actions: row => {
                  return (
                    <div className="flex flex-row items-center space-x-4">
                      <Button
                        type="button"
                        variant="contained"
                        color="secondary"
                        className="flex items-center"
                        disableElevation
                        onClick={() => updateRow(row)}
                      >
                        Edit
                      </Button>
                      <Link href={`/user-management/user-role/${row?.id}`}>
                        <IconButton>
                          <SettingsIcon className="text-gray-" />
                        </IconButton>
                      </Link>
                      <IconButton onClick={() => deleteRow(row)}>
                        <Tooltip title="Delete">
                          <DeleteIcon className="text-secondary-red" />
                        </Tooltip>
                      </IconButton>
                    </div>
                  );
                },
              }}
              sort={sort}
              sortFunction={sortFunction}
              Pagination={
                <TablePagination
                  component="div"
                  count={pagination?.total}
                  page={page}
                  rowsPerPageOptions={[10, 25]}
                  onPageChange={handleChangePage}
                  rowsPerPage={pageSize}
                  onRowsPerPageChange={handleChangePageSize}
                />
              }
            />
          </>
        )}
      </Card>
    </>
  );
};
export default List;
