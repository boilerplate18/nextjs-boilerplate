import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Button, InputLabel } from '@mui/material';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import get from 'lodash/get';

import { useModal } from '@context/modal';
import useYupValidationResolver from '@hooks/useYupValidationResolver';
import { ControlledTextField } from '@components/common/HookFormInputs';

const RoleForm = ({ selectedRow }) => {
  const { closeModal } = useModal();
  const classes = {
    inputField: 'flex flex-col w-full',
  };
  console.log('Edit the:', selectedRow);

  const validationSchema = useMemo(
    () =>
      Yup.object({
        name: Yup.string()
          .required('Required!')
          .matches(/^[a-zA-Z.]+$/, 'Must contain alphabet only!'),
      }),
    [],
  );
  const resolver = useYupValidationResolver(validationSchema);

  const { handleSubmit, formState, control } = useForm({
    reValidateMode: 'onSubmit',
    defaultValues: {},
    resolver,
  });
  const { errors, isSubmitting } = formState;

  const runSubmit = async params => {
    try {
      console.log('hit api call', { params });
    } catch (error) {
      return;
    }
  };
  return (
    <form className="space-y-4 w-full" onSubmit={handleSubmit(runSubmit)}>
      <div className={classes.inputField}>
        <InputLabel shrink htmlFor="name" required error={!!errors?.name}>
          Name
        </InputLabel>
        <ControlledTextField
          size="small"
          id="name"
          className="w-full"
          placeholder="Name"
          label=""
          variant="outlined"
          name="name"
          control={control}
          helperText={get(errors, ['name', 'message'])}
          error={!!errors?.name}
        />
      </div>
      <div className="flex flex-row space-x-4">
        <Button color="error" variant="contained" type="button" className="w-1/2 font-medium" onClick={closeModal}>
          Cancel
        </Button>
        <Button color="primary" variant="contained" type="submit" className="w-1/2 font-medium" disabled={isSubmitting}>
          {isSubmitting ? '...' : 'Save'}
        </Button>
      </div>
    </form>
  );
};
RoleForm.defaultProps = {
  selectedRow: null,
};
RoleForm.propTypes = {
  selectedRow: PropTypes.object,
};
export default RoleForm;
