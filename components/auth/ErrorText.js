import React from 'react';
import PropTypes from 'prop-types';

export default function ErrorText({ children }) {
  return (
    <>
      <p className="flex text-secondary-red tracking-wider text-xs pt-2 font-normal">{children}</p>
    </>
  );
}

ErrorText.propTypes = {
  children: PropTypes.node.isRequired,
};
