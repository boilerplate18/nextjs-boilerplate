import React from 'react';
import { useForm } from 'react-hook-form';
import { Button } from '@mui/material';
import InputPassword from './InputPassword';

const lock = '/assets/icons/lock.svg';

// components
export default function ResetPasswordForm() {
  const {
    // control,
    register,
    handleSubmit,
    formState: { errors },
    // setError,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
    context: undefined,
    shouldFocusError: true,
  });

  const actionSubmit = async () => {};

  return (
    <>
      <form onSubmit={handleSubmit(actionSubmit)} className="flex flex-col w-full" autoComplete="off">
        <InputPassword
          icon={lock}
          altIcon="Lock Password"
          tWidth="lg:w-full mb-5"
          label="Password"
          placeholder="Your New Password"
          htmlFor="password"
          register={register}
          error={errors.password}
          rules={{
            required: 'Password is required',
          }}
        />
        <InputPassword
          icon={lock}
          altIcon="Lock Password"
          tWidth="lg:w-full mb-5"
          label="Confirm Password"
          placeholder="Confirm Your New Password"
          htmlFor="password"
          register={register}
          error={errors.password}
          rules={{
            required: 'Password is required',
          }}
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="uppercase font-bold w-8/12 text-md mt-6 md:w-6/12 mx-auto"
        >
          Reset Password
        </Button>
      </form>
    </>
  );
}
