import React from 'react';
import { useForm } from 'react-hook-form';
import { Button } from '@mui/material';
import InputForm from './InputForm';

const userIcon = '/assets/icons/user_2.svg';

// components
export default function LoginForm() {
  const {
    // control,
    register,
    handleSubmit,
    formState: { errors },
    // setError,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
    context: undefined,
    shouldFocusError: true,
  });

  const actionSubmit = async () => {};

  return (
    <>
      <form onSubmit={handleSubmit(actionSubmit)} className="flex flex-col w-full" autoComplete="off">
        <InputForm
          tWidth="lg:w-full"
          label="Email"
          icon={userIcon}
          altIcon="user icon"
          placeholder="Your Username"
          htmlFor="email"
          register={register}
          error={errors.email}
          rules={{
            required: 'Email is required',
            pattern: {
              value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              message: 'Please enter a valid email',
            },
          }}
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className="uppercase font-bold w-8/12 text-md mt-6 md:w-6/12 mx-auto"
        >
          Reset Password
        </Button>
      </form>
    </>
  );
}
