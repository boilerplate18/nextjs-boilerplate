Please note that we are using `yarn` for our package manager.

## Features

- [x] Bundle Analyzer
- [x] Husky & Lint-Staged
- [x] Code-splitting
- [x] PWA ready
- [x] Import aliases
- [x] ESLint & Prettier
- [] Storybook
- [x] Tailwindcss + Styled Component Twin.Macro

## Scripts

| `yarn <script>` | Description                                                     |
| --------------- | --------------------------------------------------------------- |
| `dev`           | To start develop the app client, integrated with hot reload.    |
| `setup`         | To bootstrap the app. Use this for initial repo setup in local. |
| `build`         | To build and minify the app. App will use `prod` API.           |
| `build-dev`     | To build and minify the app. App will use `development` API.    |
| `build-staging` | To build and minify the app. App will use `staging` API.        |
| `analyze`       | To analyze the app bundle sizes. Will open in `127.0.0.1:8888`. |
| `minify:svg`    | Minify svg in enclosed specified path                           |

---

### Development

1. Clone the repository.
2. Run command `yarn setup` to bootstrap the app.
3. Run command `yarn dev`.

### Deployment

1. Clone the repository.
2. Run command `yarn build` / `yarn build-staging` / `yarn build-dev` depending on the target environment. Make sure .next folder is generated after executing this command.
3. Install PM2 on the server.
4. Run command `pm2 start yarn --name 'process_name' --interpreter bash --start` to serve `.next` folder from build command.
5. Check if the process already running `pm2 show 'proccess_name'`.
