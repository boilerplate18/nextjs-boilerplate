/* eslint-disable no-template-curly-in-string */
module.exports = {
  presets: ['next/babel'],
  plugins: ['lodash', 'babel-plugin-macros', ['styled-components', { ssr: true }]],
  env: {
    production: {
      plugins: [
        [
          'transform-react-remove-prop-types',
          {
            mode: 'remove',
            removeImport: true,
            additionalLibraries: ['prop-types-extra'],
            ignoreFilenames: ['node_modules'],
          },
        ],
        [
          'babel-plugin-import',
          {
            libraryName: '@mui/material',
            // Use "'libraryDirectory': ''," if your bundler does not support ES modules
            libraryDirectory: '',
            camel2DashComponentName: false,
          },
          'core',
        ],
        [
          'babel-plugin-import',
          {
            libraryName: '@mui/icons-material',
            // Use "'libraryDirectory': ''," if your bundler does not support ES modules
            libraryDirectory: '',
            camel2DashComponentName: false,
          },
          'icons',
        ],
      ],
    },
  },
};
